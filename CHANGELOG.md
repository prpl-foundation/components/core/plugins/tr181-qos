# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v4.5.0 - 2024-12-16(09:21:51 +0000)

### Other

- [QoS] Add iptables-physdev compatibility

## Release v4.4.0 - 2024-11-28(11:49:23 +0000)

### Other

- [QoS] It should be possible to specify interfaces using alias paths

## Release v4.3.3 - 2024-11-27(09:34:42 +0000)

### Other

- - pbit & DSCP remarking

## Release v4.3.2 - 2024-09-18(11:16:21 +0000)

### Other

- [QoS Classification] Wrong log messages about activating of a classification rule

## Release v4.3.1 - 2024-09-05(12:36:35 +0000)

## Release v4.3.0 - 2024-09-04(05:26:09 +0000)

### Other

- - [QoS][CHR2fB] Apply [Mxl] Add prio and tbf qdiscs support to mod-qos-tc

## Release v4.2.7 - 2024-08-22(13:43:54 +0000)

### Other

- - Prpl vendor module for QoS manager

## Release v4.2.6 - 2024-08-21(11:46:23 +0000)

### Other

- Add extra unit test for double gmap query cb

## Release v4.2.5 - 2024-08-21(11:02:28 +0000)

### Other

- - Prpl vendor module for QoS manager

## Release v4.2.4 - 2024-08-16(08:05:11 +0000)

### Other

- Fix problem with double gmap query callback

## Release v4.2.3 - 2024-07-29(08:19:40 +0000)

### Other

- - [amx] Failing to restart processes with init scripts

## Release v4.2.2 - 2024-07-24(13:18:03 +0000)

## Release v4.2.1 - 2024-07-17(09:36:03 +0000)

### Fixes

- [QoS] Crash during startup

### Other

- [QoS] Tests are failing after changes to libfwrules

## Release v4.2.0 - 2024-06-05(13:22:23 +0000)

### New

- [VLAN0][STB Detection] It must be possible to Configure the switch with MAC Information

## Release v4.1.4 - 2024-05-22(20:44:17 +0000)

### Fixes

- - QoS crash

## Release v4.1.3 - 2024-04-25(08:28:04 +0000)

### Other

- [QoS] Update licenses for device based QoS

## Release v4.1.2 - 2024-04-10(07:11:32 +0000)

### Changes

- Make amxb timeouts configurable

## Release v4.1.1 - 2024-04-05(09:12:12 +0000)

### Other

- [QoS] Make device based QoS available on open source

## Release v4.1.0 - 2024-04-04(07:48:53 +0000)

### Fixes

- [QOS]classification instance with vendorClassId is not always applied

## Release v4.0.3 - 2024-03-23(13:22:22 +0000)

### New

- Add tr181-device proxy odl files to components

## Release v4.0.2 - 2024-03-20(06:51:03 +0000)

### Other

- Add missing odl file for device based QoS

## Release v4.0.1 - 2024-03-19(07:43:38 +0000)

### Other

- Rework TR-181 interface stacks indexing number

## Release v4.0.0 - 2024-03-18(12:21:07 +0000)

### Breaking

- [QoS] Interface and direction are needed for correct classifications

## Release v3.4.2 - 2024-03-13(10:13:07 +0000)

### Fixes

- Device based QoS is not always supported

## Release v3.4.1 - 2024-03-08(08:11:59 +0000)

### Other

- Fix CI dependencies

## Release v3.4.0 - 2024-03-07(19:15:53 +0000)

### New

- [QoS][Classification] It must be possible to allow QoS classification based on UserClassID, VendorClassID

## Release v3.3.17 - 2024-02-23(15:55:59 +0000)

### Other

- - [tr181-qos] - Update documentation

## Release v3.3.16 - 2024-02-19(09:12:52 +0000)

### Other

- - [tr181-qos] - Remove unused functions

## Release v3.3.15 - 2024-02-13(09:13:54 +0000)

### Fixes

- Multilevel node classification

## Release v3.3.14 - 2024-02-09(08:01:44 +0000)

### Other

- [QOS] Support hardware based QoS CLASSIFY target

## Release v3.3.13 - 2023-12-22(10:09:40 +0000)

### Other

- [QOS] Support hardware based QoS

## Release v3.3.12 - 2023-11-29(11:55:51 +0000)

### Fixes

- Fix QoS startup error when no defaults.d directory is available

## Release v3.3.11 - 2023-10-25(10:34:47 +0000)

### Other

- - [prpl][qos] Activate child nodes only when the parent node is successfully activated

## Release v3.3.10 - 2023-10-19(12:19:36 +0000)

### Other

- - [tr181-qos] Plugin crashes when defaults file is empty

## Release v3.3.9 - 2023-10-18(15:06:33 +0000)

## Release v3.3.8 - 2023-10-18(14:52:38 +0000)

### Changes

- Remove odl defaults

## Release v3.3.7 - 2023-10-13(14:13:49 +0000)

### Changes

-  All applications using sahtrace logs should use default log levels

## Release v3.3.6 - 2023-10-12(13:02:55 +0000)

### Other

- [TR-181][QOS] crash when no QoS datamodel available

## Release v3.3.5 - 2023-08-04(09:39:44 +0000)

### Changes

- Move odl configuration to board-configurator

## Release v3.3.4 - 2023-05-09(14:08:33 +0000)

### Other

- - [TR-181]gsdm is not working properly on Device.QoS.Classification

## Release v3.3.3 - 2023-03-15(14:56:46 +0000)

### Other

- [Config] enable configurable coredump generation

## Release v3.3.2 - 2023-03-02(12:47:10 +0000)

### Changes

- - [QoS] Controller parameters must be protected

## Release v3.3.1 - 2023-02-24(15:42:30 +0000)

### Other

- [CI] Add missing dependency on libqosmodule

## Release v3.3.0 - 2023-02-06(15:44:14 +0000)

### Changes

- - [QOS] Add support for defaults.d directory in odl

## Release v3.2.0 - 2023-02-03(12:57:09 +0000)

### New

- - [libqosmodule] Integrate library

## Release v3.1.0 - 2023-01-25(09:54:50 +0000)

### Changes

- [tr181-qos] random: QoS.Queue reported as misconfigured

## Release v3.0.9 - 2023-01-10(11:45:52 +0000)

### Fixes

- avoidable copies of strings, htables and lists

## Release v3.0.8 - 2022-11-25(15:25:47 +0000)

### Fixes

- - [VLAN][QOS][WNC] Changing the WANMode to vlan, QoS queues creating will fail
- fail") added the netmodel flag 'eth_intf' to the

## Release v3.0.7 - 2022-11-23(09:45:19 +0000)

### Other

- - [tr181-qos] Update documentation

## Release v3.0.6 - 2022-11-02(09:58:36 +0000)

### Fixes

- - [VLAN][QOS][WNC] Changing the WANMode to vlan, QoS queues creating will fail

## Release v3.0.5 - 2022-10-10(09:38:40 +0000)

### Fixes

- - [QoS] Queues are in state Error if the wan cable is not connected

## Release v3.0.4 - 2022-10-10(08:58:35 +0000)

### Fixes

- - [QoS] Queues are in state Error if the wan cable is not connected

## Release v3.0.3 - 2022-09-22(16:13:59 +0000)

### Fixes

- - [tr181-qos] Do not require on NetModel.Intf

## Release v3.0.2 - 2022-09-14(06:55:54 +0000)

### Fixes

- - [QoS] Segmentation Error when trying to access QoS. datamodel

## Release v3.0.1 - 2022-09-12(14:14:25 +0000)

### Fixes

- Enable core dumps by default

## Release v3.0.0 - 2022-09-08(07:39:20 +0000)

### New

- - [tr181-qos] Integrate netmodel to translate interfaces

## Release v2.1.1 - 2022-09-05(12:59:15 +0000)

### New

- - [prpl][qos] Expand unit tests for node creation

## Release v2.1.0 - 2022-09-01(10:07:51 +0000)

### New

- - [prpl][qos] Integrate new TR-181 parameters for Device.QoS v2.16

## Release v2.0.1 - 2022-08-19(14:28:29 +0000)

## Release v2.0.0 - 2022-08-03(08:38:34 +0000)

## Release v1.1.11 - 2022-07-01(08:01:34 +0000)

### Fixes

- - [tr181-qos][Regression] Queue.TrafficClasses values unexpectedly overwritten

## Release v1.1.10 - 2022-05-02(09:32:55 +0000)

### Fixes

- - [tr181-qos] Set realistic shaping rates for the default configuration

## Release v1.1.9 - 2022-03-24(09:27:55 +0000)

### Changes

- [GetDebugInformation] Add data model debuginfo in component services

## Release v1.1.8 - 2022-02-25(09:04:07 +0000)

### Other

- Update documentation
- Update documentation

## Release v1.1.7 - 2022-02-24(09:22:01 +0000)

### Fixes

- - [prpl][qos] QueueKey must be used as queue identifier

## Release v1.1.6 - 2022-02-17(15:17:17 +0000)

### Changes

- - [tr181-qos] Rework variants for tc module

## Release v1.1.5 - 2022-02-08(11:18:22 +0000)

### Other

- - [tr181-qos] Document code
- - [tr181-qos] Document code

## Release v1.1.4 - 2022-02-04(10:07:00 +0000)

### Changes

- - [prpl][qos] Startup order does not work

## Release v1.1.3 - 2022-01-26(15:51:33 +0000)

### Changes

- - [tr181-qos] move classificationkey to queuekey and add interface parameter to scheduler

## Release v1.1.2 - 2022-01-21(08:51:02 +0000)

### Changes

- - [tr181-qos] Remove X_PRPL prefixes

## Release v1.1.1 - 2022-01-03(14:07:50 +0000)

### Changes

- license: change to BSD-2-Clause-Patent license

### Other

- - Use BSD-2-Clause-Patent license

## Release v1.1.0 - 2021-12-14(10:51:58 +0000)

### New

- queuestats: unit tests

### Other

- - Adding unit tests for queue stats

## Release v1.0.7 - 2021-12-13(11:14:44 +0000)

### Changes

- node: Adapt nodes default config and make children and parents not protected.
- nodes: Use mod_dmext

## Release v1.0.6 - 2021-12-10(14:22:08 +0000)

### Changes

- tr181-qos: Make controller and supportedControllers parameter visible in dm

## Release v1.0.5 - 2021-11-25(09:31:00 +0000)

### Other

- [ACL] Add default ACL file for the QoS Manager

## Release v1.0.4 - 2021-11-17(11:55:21 +0000)

### Other

- Opensource component

## Release v1.0.3 - 2021-11-16(10:18:27 +0000)

### Changes

- trace: do not print pointers in the log

### Other

- - [tr181-qos] Cleanup logging

## Release v1.0.2 - 2021-11-16(09:57:08 +0000)

### Changes

- odl: set log level to 200

### Other

- - [tr181-qos] Set default log level to 200

## Release v1.0.1 - 2021-11-10(12:57:37 +0000)

### Changes

- tr181-qos: queuestats for qos-core

## Release v1.0.0 - 2021-11-05(14:09:17 +0000)

### Breaking

- - [tr181-qos] Re-work variant to (de-)activate a queue

### New

- - [tr181-qos] Support for ordered classifications
- node: functions to query a list of parents or children by type

### Changes

- cleanup: remove X_PRPL_ references
- qos: remove unnecessary functions between controller and qos
- queue: proper return values

### Other

- Support for queue (de)activation
- - [tr181-qos] Remove X_PRPL_ references
- - Remove layer between controller and qos
- - [tr181-qos] functions to query a list of parents and children by type

## Release v0.2.0 - 2021-10-11(15:45:23 +0000)

### New

- node: add functions to find a parent or child node by type

### Other

- - [tr181-qos] Add functions to find a parent or child node by type

## Release v0.1.0 - 2021-10-11(14:13:45 +0000)

### New

- node: link nodes to lower layer objects

### Fixes

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components

### Other

- [tr181 plugins][makefile] Dangerous clean target for all tr181 components
- [tr181-qos] Add public header directory
- [tr181-qos] Custom QoS.X_PRPL_Node object
- [Gitlab CI][Unit tests][valgrind] Pipelines don't stop when memory leaks are detected
- [tr181-qos] Custom QoS.X_PRPL_Scheduler object
- Remove X_PRPL in Filenames, Functions and Member vars
- - tr181-qos: Integrating separation of concerns
- - [tr181-qos] Replace data model functions to retrieve an object
- - [tr181-qos] Enable all node types
- - [tr181-qos] Link nodes to lower layer objects

## Release 0.0.2 - 2021-05-26(18:26:42 +0200)

- Fixed compile issue.

## Release 0.0.1 - 2021-05-07(14:47:37 +0200)

### Added

- dm-qos-queue.{h,c} - Event handeling for new, removed or changed Device.QoS.Queue.{i} instances.
- qos-queue.{h,c} - The internal representation of a Device.QoS.Queue.{i} instance.
- qos.{h,c} - The internal representation of a Device.QoS object.
- qos-controller.{h,c} - Initializes the structures qos_t, qos_queue_t.
- test directory - Test and coverage reports are added for qos.h and qos-queue.h


