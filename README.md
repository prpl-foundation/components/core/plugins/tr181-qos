# tr181-qos

[TOC]

# Introduction

`tr181-qos` implements the TR-181 Device.QoS data model. The data model is based on *TR-181
Issue: 2 Amendment 16* and the specification can be found on the [BroadBand
Forum](https://www.broadband-forum.org/)'s website. The data model is supported by protocols
like [CWMP](https://cwmp-data-models.broadband-forum.org/) or
[USP](https://usp-data-models.broadband-forum.org/).

TR-181 Device.QoS defines objects and parameters to describe Quality of Service (QoS)
configurations on the CPE. These objects and parameters form a subset of the *Device:2* root
data model and can be consulted:
- [Root data model for
  CWMP](https://cwmp-data-models.broadband-forum.org/#Device:2)
- [Root data model for
  USP](https://usp-data-models.broadband-forum.org/#Device:2)

Different parts of a QoS configuration can be described using the data model.  These parts are
divided into classifications, queues, shapers, schedulers, etc.

TR-181 Device.QoS defines the following objects:

| Object                            | Implemented in tr181-qos |
| --------------------------------- | ------------------------ |
| Device.QoS.                       | **Yes**                  |
| Device.QoS.Classification.{i}.    | **Yes, a subset**        |
| Device.QoS.App.{i}.               | No                       |
| Device.QoS.Flow.{i}.              | No                       |
| Device.QoS.Policer.{i}.           | No                       |
| Device.QoS.Queue.{i}.             | **Yes**                  |
| Device.QoS.QueueStats.{i}.        | **Yes**                  |
| Device.QoS.Shaper.{i}.            | **Yes**                  |
| Device.QoS.Scheduler.{i}.         | **Yes**                  |

@attention
This implementation also defines custom objects, which are not part of TR-181.

| Custom Object                     |
| --------------------------------- |
| Device.QoS.Node.{i}.              |

The use of all objects is described futher in this documentation.

# Overview

The next diagram shows a layer overview of the QoS subsystem. To facilitate QoS, several layers have
been implemented:
 - Data model layer.
 - Ambiorix layer.
 - QoS common layer (this component).
 - QoS specific layer.
 - QoS library layer.
 - Firewall library layer.
 - Userspace library layer.
 - Operating System (OS) or Board Support Package (BSP) layer.
 - Hardware layer.

![High level simple_layer diagram](doc/images/architecture_simple_layered_diagram.svg)

By expanding the layers, a high level view of the components can be
shown in the next diagram. The layers are explained briefly below the image.
For more details, check the [architecture](@ref architecture) page.

![High level layer diagram](doc/images/architecture_layered_diagram.svg)

## Data model layer

The data model layer often provides a multitude of Object Definition Language (ODL) files.
This group of ODL files describes the data model, which is imported by this component.

An ODL file features a subset of the TR-181 Device.QoS data model specification as described by
the BroadBand Forum. In general, one ODL file exists for each object. E.g.
Device.QoS.Classification, Device.QoS.Queue, etc.

## Ambiorix layer

The Ambiorix layer, abbreviated by **amx**, is the reference framework in prplOS to build
applications.  Ambiorix consists of several libraries, providing data collections, data model
parsers and the ability to connect to different bus systems. For example, the ODL files from
the data model layer are parsed and validated by amx. A good comprehension of amx is required
to leverage all of its features. Consult the Ambiorix documentation for more info.

## QoS common layer

The QoS common layer refers to tr181-qos, the component described in this documentation. The
[architecture](@ref architecture) page describes this component in more detail. In essence,
this is what it consists of:

* **Core component**: the heart of the tr181-qos application and interacts with the data model
  layer via Ambiorix libraries. The core has several responsibilities:

    - Data model layer event handling, e.g. addition/deletion/altering of data model instances.
    - Update the data model layer state.
    - Convert the data model layer parameters into a set of parameters that can be used by the
      lower level layers.
    - Network event handling via `libnetmodel`. E.g., a network interface state transitions to
      down and as consequence, a particular QoS instance, like a classification or queue, has
      to be disabled.

- **Classification component**: the classification component performs the translation from a
  Device.QoS.Classification.{i} instance to lower level firewall parameters, which are used to
  add, delete or update firewall rules. To achieve this, the libraries provided by the Firewall
  library layer are used.

- **Queue/QueueStats/Shaper/Scheduler components**: these components process the following data
  model instances:

    - Device.QoS.Queue.{i}
    - Device.QoS.QueueStats.{i}
    - Device.QoS.Shaper.{i}
    - Device.QoS.Scheduler.{i}

  The commonalities between the components are:
    - Convert the data model instance parameters to a format, suitable to be processed by the
      QoS specific layer and QoS library layer.
    - Dispatch tasks to the QoS specific layer, for example, activate a queue, reconfigure a
      shaper or query queue statistics.
    - Notify the core component about the status of an object and update the data model
      accordingly.

## QoS library layer

The QoS library layer provides a set of libraries that the QoS common layer and the QoS
specific layer can use to interact with each other.

- **libqoscommon**: a library grouping common functionality for Shapers, Schedulers, Queues and
  QueueStats.

- **libqosnode**: a library providing an abstraction layer for Shapers, Schedulers, Queues and
  QueueStats, and the possibility to have a hierarchical QoS configuration.

- **libqosmodule**: a library providing an abstraction layer between the QoS common and QoS
  specific layer. The core component asks the specific layer to execute a function via this
  library.

## QoS specific layer

The QoS specific layer is characterized by one or more modules. Modules are standalone plugins
(shared object files), loaded at startup.  A module implements the low-level API, used to
execute QoS operations to the operating system and hardware. A default module for Linux Traffic
Control, **mod-qos-tc**, is available. Other modules can be developed when required. Suppose a
vendor does not (yet) support Linux Traffic Control. In that case, a new module can implement
this vendor interface for QoS. Multiple modules can run in parallel. There are no limitations.

The list of modules to load at startup can be set by the comma-separated
`SupportedControllers` parameter, found in the configuration ODL defaults file.

Each data model instance uses the Linux Traffic Control module by default, but
this can be overwritten by defining a `Controller` parameter for that instance.
The `Controller` parameter value must be present in the `SupportedControllers`
parameter list.

More info can be found at the [module](@ref module) page.

## Userspace library layer

The userspace library layer represent the libraries, provided by the open source community or
the hardware vendor. The QoS specific layer and Firewall library layer leverage these libraries
to interface with the operating system and the hardware.

## OS layer

The OS layer or operating system layer, often Linux, receives instructions from the userspace
library layer via IOCTLs, netlink, sysfs, procfs or any other interface. The instructions are
verified in the kernel and forwarded to the hardware via the drivers.

## Hardware layer

The hardware layer represents the physical hardware or System-on-Chip (SoC)
that features WAN interfaces, like xPON or xDSL. The hardware usually contains
some sort of packet accelerator, used to offload network packets from the CPU.
The operating system layer is the only layer that directly interfaces with the
hardware.

# Building, installing and testing

## Docker container

You could install all tools needed for testing and developing on your local
machine, or use a pre-configured environment. Such an environment is already
prepared for you as a docker container.

1. Install Docker

    Docker must be installed on your system. Here are some links that could
    help you:

    - [Get Docker Engine - Community for
      Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
    - [Get Docker Engine - Community for
      Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    - [Get Docker Engine - Community for
      Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    - [Get Docker Engine - Community for
      CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
      <br /><br />

    Make sure you user id is added to the docker group:

        sudo usermod -aG docker $USER
    <br />

2. Fetch the container image

    To get access to the pre-configured environment, pull the image and launch
    a container.

    Pull the image:

        docker pull registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    Before launching the container, you should create a directory which will be
    shared between your local machine and the container.

        mkdir -p ~/amx/qos/applications/

    Launch the container:

        docker run -ti -d \
            --name oss-dbg \
            --restart=always \
            --cap-add=SYS_PTRACE \
            --cap-add=CAP_NET_RAW \
            --cap-add=CAP_NET_ADMIN \
            --sysctl net.ipv6.conf.all.disable_ipv6=1 \
            -e "USER=$USER" \
            -e "UID=$(id -u)" \
            -e "GID=$(id -g)" \
            -v ~/amx:/home/$USER/amx \
            registry.gitlab.com/soft.at.home/docker/oss-dbg:latest

    The `-v` option bind mounts the local directory for the amx project in
    the container, at the exact same place.
    The `-e` options create environment variables in the container. These
    variables are used to create a user name with exactly the same user id and
    group id in the container as on your local host (user mapping).

    You can open as many terminals/consoles as you like:

        docker exec -ti --user $USER oss-dbg /bin/bash

## Building

### Prerequisites

<code>tr181-qos</code> depends on the following libraries:

- libamxc
- libamxo
- libamxp
- libamxd
- libamxm
- libsahtrace
- libfwrules
- libfwinterface
- libnetmodel
- libqoscommon
- libqosnode
- libqosmodule

These libraries and additional packages can be installed in the container:

    sudo apt-get update
    sudo apt-get install libamxc libamxo libamxp libamxd libamxm amxo-cg \
    amxo-xml-to amxrt mod-dmext mod-amxb-ubus mod-ba-cli mod-dm-cli \
    mod-sahtrace sah-lib-sahtrace-dev libfwrules libfwinterface libnetmodel \
    libqoscommon libqosnode libqosmodule iptables iproute2

The `iproute2` is installed for later purposes and provides the `ip` command.

When tr181-qos is used, it is possible to compile it with support for
device based classification. This requires the installation of `libgmap-client`,
which is also available as a debian package:

    sudo apt-get install libgmap-client

### Build tr181-qos

1. Clone the git repository

    To be able to build it, you need the source code. So open the directory
    just created for the ambiorix project and clone this library in it (on your
    local machine).

        cd ~/amx/qos/applications/
        git clone git@gitlab.com:prpl-foundation/components/core/plugins/tr181-qos.git

2. Build it

    When using the internal gitlab, you must define an environment variable
    `VERSION_PREFIX` before building in your docker container.

        export VERSION_PREFIX="master_"

    After the variable is set, you can build the package.

        cd ~/amx/qos/applications/tr181-qos
        make
    Alternatively, you can compile it with support for device based
    classification by using an extra configuration variable

        CONFIG_SAH_AMX_TR181_QOS_DEVICE_BASED_CLASSIFICATION=y make

## Installing

### Using make target install

You can install your own compiled version easily in the container by running
the install target.

    cd ~/amx/qos/applications/tr181-qos
    sudo -E make install

### Using package

From within the container you can create packages.

    cd ~/amx/qos/applications/tr181-qos
    make package

The packages generated are:

    ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.tar.gz
    ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.deb

You can copy these packages and extract/install them.

For Ubuntu or Debian distributions use dpkg:

    sudo dpkg -i ~/amx/qos/applications/tr181-qos/tr181-qos-<VERSION>.deb

## Testing

### Prerequisites

No extra components are needed for testing `tr181-qos`.

### Run tests

You can run the tests by executing the following command:

    cd ~/amx/qos/applications/tr181-qos/test
    make

Or this command if you also want the coverage tests to run:

    cd ~/amx/qos/applications/tr181-qos/test
    make run coverage

You can combine both commands:

    cd ~/amx/qos/applications/tr181-qos
    make test

### Coverage reports

The coverage target will generate coverage reports using
[gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html) and
[gcovr](https://gcovr.com/en/stable/guide.html).

A summary for each c-file is printed in your console after the tests are run.
A HTML version of the coverage reports is also generated. These reports are
available in the output directory of the compiler used.  For example, when using
native gcc, the output of `gcc -dumpmachine` is `x86_64-linux-gnu`, the HTML
coverage reports can be found at
`~/amx/qos/applications/tr181-qos/output/x86_64-linux-gnu/coverage/report.`

You can easily access the reports in your browser. In the container start a
python3 http server in background.

    cd ~/amx/
    python3 -m http.server 8080 &

Use the following url to access the reports:

    http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/<MACHINE>/coverage/report

You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/applications/tr181-qos$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is:
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/x86_64-linux-gnu/coverage/report/`

## Documentation

### Prerequisites

To generate the documentation, [Doxygen](https://www.doxygen.nl) is required
and already available in the container. In case you want to install this on
your local machine:

    sudo apt-get install doxygen

### Paths

The documentation is split in two parts, starting from the repository path:

    cd ~/amx/qos/applications/tr181-qos

Here, you can find:

- README.md: this file is used on Gitlab and is the main page for the Doxygen documentation.
- docs directory: contains the Doxygen settings, code examples and additional pages used in Doxygen.

The code itself is documented in the approriate header and source files.

### Generate documentation

You can generate the documentation by executing the following command:

    cd ~/amx/qos/applications/tr181-qos
    make doc

The full documentation is available in the `output` directory. You can easily
access the documentation in your browser. As described in the coverage reports
section, you can start a http server in the container.

    cd ~/amx/
    python3 -m http.server 8080 &

Note that there are two different kinds of documentation:
- Doxygen documentation, describing the internals of the TR-181 QoS Manager.
- ODL documentation, describing the TR-181 Device.QoS data model.

Use the following urls to access the reports:

- Doxygen documentation:

        http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/doc/doxy-html/index.html

- TR-181 data model documentation:

        http://<IP ADDRESS OF YOUR CONTAINER>:8080/qos/applications/tr181-qos/output/html/index.html


You can find the ip address of your container by using the `ip -br a` command
in the container.

Example:

    USER@<CID>:~/amx/qos/applications/tr181-qos$ ip -br a
    lo              UNKNOWN        127.0.0.1/8
    eth0            UP             172.17.0.3/16


In this case the ip address of the container is `172.17.0.3`.  So the url you
should use is
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/doc/doxy-html/index.html`
or
`http://172.17.0.3:8080/qos/applications/tr181-qos/output/html/index.html`

# What's next?

Consult the full documentation for a complete description of the architecture,
the module API, example configurations, etc.
