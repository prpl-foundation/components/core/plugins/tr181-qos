/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_CLASSIFICATION_DEVICE_H__)
#define __QOS_CLASSIFICATION_DEVICE_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include <fwrules/fw_rule.h>

#include <gmap/gmap.h>

#include "qos-classification.h"

#define SIG_DEVICE_UPDATED "classification-device-action"

struct _qos_classification_device {
    gmap_query_t* query_src;
    gmap_query_t* query_dst;
    amxc_var_t device_rules_src;    // Variant of type htable, key is device alias, value is list of device IPs
    amxc_var_t device_rules_dst;    // Variant of type htable, key is device alias, value is list of device IPs
};

static inline amxc_var_t* qos_classification_device_get_device_rules_src(qos_classification_device_t* const class_device) {
    return class_device == NULL ? NULL : &class_device->device_rules_src;
}
static inline amxc_var_t* qos_classification_device_get_device_rules_dst(qos_classification_device_t* const class_device) {
    return class_device == NULL ? NULL : &class_device->device_rules_dst;
}
static inline qos_classification_device_t* qos_classification_device_get_class_device(const qos_classification_t* const classification) {
    return classification == NULL ? NULL : classification->class_device;
}

int _qos_classification_device_entrypoint(int reason,
                                          amxd_dm_t* dm,
                                          amxo_parser_t* parser);

int qos_classification_device_based_activate(const qos_classification_t* const classification);

void qos_classification_device_based_deactivate(const qos_classification_t* const classification);

#ifdef __cplusplus
}
#endif

#endif // __QOS_CLASSIFICATION_DEVICE_H__

