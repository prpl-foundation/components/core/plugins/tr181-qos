/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#if !defined(__QOS_CONFIG_H__)
#define __QOS_CONFIG_H__

#ifdef __cplusplus
extern "C"
{
#endif

#include "tr181-qos.h"

typedef struct _qos_config_iptables_target {
    const char* ipv4_target;
    const char* ipv6_target;
} qos_config_iptables_target;

#define QOS_CONFIG_IPTABLES_PREROUTING_TARGET "prerouting-target"
#define QOS_CONFIG_IPTABLES_INPUT_TARGET "input-target"
#define QOS_CONFIG_IPTABLES_FORWARD_TARGET "forward-target"
#define QOS_CONFIG_IPTABLES_POSTROUTING_TARGET "postrouting-target"
#define QOS_CONFIG_IPTABLES_OUTPUT_TARGET "output-target"

#define QOS_CONFIG_IP6TABLES_PREROUTING_TARGET "prerouting6-target"
#define QOS_CONFIG_IP6TABLES_INPUT_TARGET "input6-target"
#define QOS_CONFIG_IP6TABLES_FORWARD_TARGET "forward6-target"
#define QOS_CONFIG_IP6TABLES_POSTROUTING_TARGET "postrouting6-target"
#define QOS_CONFIG_IP6TABLES_OUTPUT_TARGET "output6-target"

const char* qos_config_get_iptables_target(const char* param);
const char* qos_config_get_ip6tables_target(const char* param);

static inline const char* qos_config_get_name(void) {
    return GET_CHAR(qos_get_config(), "name");
}

amxc_string_t* qos_config_get_prefixed_parameter(const char* param);

#ifdef __cplusplus
}
#endif

#endif // __QOS_CONFIG_H__
