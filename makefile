include makefile.inc

NOW = $(shell date +"%Y-%m-%d(%H:%M:%S %z)")

# Extra destination directories
PKGDIR = ./output/$(MACHINE)/pkg/

define create_changelog
	@$(ECHO) "Update changelog"
	mv CHANGELOG.md CHANGELOG.md.bak
	head -n 9 CHANGELOG.md.bak > CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(ECHO) "## Release $(VERSION) - $(NOW)" >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	$(GIT) log --pretty=format:"- %s" $$($(GIT) describe --tags | grep -v "merge" | cut -d'-' -f1)..HEAD  >> CHANGELOG.md
	$(ECHO) "" >> CHANGELOG.md
	tail -n +10 CHANGELOG.md.bak >> CHANGELOG.md
	rm CHANGELOG.md.bak
endef

# targets
all:
	$(MAKE) -C src all

clean:
	$(MAKE) -C src clean
	$(MAKE) -C test clean

install: all
	$(INSTALL) -D -p -m 0644 odl/tr181-qos.odl $(DEST)/etc/amx/tr181-qos/tr181-qos.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-classification.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-classification.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-queue.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-queue.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-queuestats.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-queuestats.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-shaper.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-shaper.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos_definition.odl $(DEST)/etc/amx/tr181-qos/tr181-qos_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(DEST)/etc/amx/tr181-device/extensions/01_device-qos_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-scheduler.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-scheduler.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-node.odl $(DEST)/etc/amx/tr181-qos/tr181-qos-node.odl
ifeq ($(CONFIG_SAH_AMX_TR181_QOS_DEVICE_BASED_CLASSIFICATION),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)-classification-device.odl $(DEST)/etc/amx/$(COMPONENT)/$(COMPONENT)-classification-device.odl
endif
	$(INSTALL) -d -m 0755 $(D)//etc/amx/tr181-qos/defaults.d
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(DEST)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/tr181-qos.so $(DEST)/usr/lib/amx/tr181-qos/tr181-qos.so
	$(INSTALL) -d -m 0755 $(DEST)$(BINDIR)
	ln -sfr $(DEST)$(BINDIR)/amxrt $(DEST)$(BINDIR)/tr181-qos
	$(INSTALL) -D -p -m 0755 scripts/tr181-qos.sh $(DEST)$(INITDIR)/$(COMPONENT)

package: all
	$(INSTALL) -D -p -m 0644 odl/tr181-qos.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-classification.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-classification.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-queue.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-queue.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-queuestats.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-queuestats.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-shaper.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-shaper.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos_definition.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos_definition.odl
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)_mapping.odl $(PKGDIR)/etc/amx/tr181-device/extensions/01_device-qos_mapping.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-scheduler.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-scheduler.odl
	$(INSTALL) -D -p -m 0644 odl/tr181-qos-node.odl $(PKGDIR)/etc/amx/tr181-qos/tr181-qos-node.odl
ifeq ($(CONFIG_SAH_AMX_TR181_QOS_DEVICE_BASED_CLASSIFICATION),y)
	$(INSTALL) -D -p -m 0644 odl/$(COMPONENT)-classification-device.odl $(PKGDIR)/etc/amx/$(COMPONENT)/$(COMPONENT)-classification-device.odl
endif
	$(INSTALL) -d -m 0755 $(D)//etc/amx/tr181-qos/defaults.d
	$(INSTALL) -D -p -m 0660 acl/admin/$(COMPONENT).json $(PKGDIR)$(ACLDIR)/admin/$(COMPONENT).json
	$(INSTALL) -D -p -m 0644 output/$(MACHINE)/object/tr181-qos.so $(PKGDIR)/usr/lib/amx/tr181-qos/tr181-qos.so
	$(INSTALL) -d -m 0755 $(PKGDIR)$(BINDIR)
	rm -f $(PKGDIR)$(BINDIR)/tr181-qos
	ln -sfr $(PKGDIR)$(BINDIR)/amxrt $(PKGDIR)$(BINDIR)/tr181-qos
	$(INSTALL) -D -p -m 0755 scripts/tr181-qos.sh $(PKGDIR)$(INITDIR)/$(COMPONENT)
	cd $(PKGDIR) && $(TAR) -czvf ../$(COMPONENT)-$(VERSION).tar.gz .
	cp $(PKGDIR)../$(COMPONENT)-$(VERSION).tar.gz .
	make -C packages

changelog:
	$(call create_changelog)

doc:
	mkdir -p output/doc
	VERSION=$(VERSION) doxygen doc/tr181-qos.doxy

	$(eval ODLFILES += odl/tr181-qos-classification.odl)
	$(eval ODLFILES += odl/tr181-qos-queue.odl)
	$(eval ODLFILES += odl/tr181-qos-queuestats.odl)
	$(eval ODLFILES += odl/tr181-qos-shaper.odl)
	$(eval ODLFILES += odl/tr181-qos_definition.odl)
	$(eval ODLFILES += odl/tr181-qos-scheduler.odl)
	$(eval ODLFILES += odl/tr181-qos-node.odl)

	mkdir -p output/xml
	mkdir -p output/html
	mkdir -p output/confluence
	amxo-cg -Gxml,output/xml/$(COMPONENT).xml $(or $(ODLFILES), "")
	amxo-xml-to -x html -o output-dir=output/html -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml
	amxo-xml-to -x confluence -o output-dir=output/confluence -o title="$(COMPONENT)" -o version=$(VERSION) -o sub-title="Datamodel reference" output/xml/*.xml

test:
	$(MAKE) -C test run
	$(MAKE) -C test coverage

.PHONY: all clean changelog install package doc test