/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_action.h>

#include "tr181-qos.h"
#include "qos-assert.h"
#include "qos-controller.h"
#include "dm-qos-node.h"
#include <qosnode/qos-node.h>

#include <stdio.h>

static int qos_add_node_to_list(amxc_var_t* params, amxd_object_t* list) {
    amxc_var_t args, ret;
    amxd_status_t status;
    int retval = -1;

    amxc_var_init(&args);
    amxc_var_init(&ret);
    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(amxc_htable_t, &args, "parameters", amxc_var_constcast(amxc_htable_t, params));
    amxc_var_add_key(uint32_t, &args, "access", amxd_dm_access_protected);

    status = amxd_action_object_add_inst(list, NULL, action_object_add_inst, &args, &ret, NULL);
    if((status == amxd_status_ok) || (status == amxd_status_duplicate)) {
        retval = 0;
    }

    amxc_var_clean(&ret);
    amxc_var_clean(&args);

    return retval;
}

static int qos_create_node_relation(const char* node_name, amxd_object_t* node, const char* relation) {
    amxc_string_t list_path;
    amxc_var_t params;
    amxd_object_t* list;
    char* path = NULL;
    int retval = -1;

    amxc_string_init(&list_path, 0);
    amxc_var_init(&params);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    amxc_var_add_key(cstring_t, &params, "Name", node_name);
    path = amxd_object_get_path(node, 0);
    amxc_string_setf(&list_path, "%s.%s", path, relation);
    list = qos_get_object(amxc_string_get(&list_path, 0));
    when_null(list, exit);
    retval = qos_add_node_to_list(&params, list);
    when_failed(retval, exit);

exit:
    free(path);
    amxc_var_clean(&params);
    amxc_string_clean(&list_path);
    return retval;
}

static int qos_create_node_relations(amxd_object_t* node, const char* children) {
    int retval = -1;
    amxd_object_t* child, * node_child;
    amxc_string_t child_alias, node_path;

    amxc_string_init(&child_alias, 0);
    amxc_string_init(&node_path, 0);

    child = qos_get_object(children);
    when_null(child, exit);

    amxc_string_setf(&child_alias, "node-%s", GET_CHAR(amxd_object_get_param_value(child, "Alias"), NULL));
    amxc_string_setf(&node_path, "QoS.Node.%s", amxc_string_get(&child_alias, 0));
    node_child = qos_get_object(amxc_string_get(&node_path, 0));
    when_null(node_child, exit);

    retval = qos_create_node_relation(amxc_string_get(&child_alias, 0), node, "Child");
    when_failed(retval, exit);
    retval = qos_create_node_relation(GET_CHAR(amxd_object_get_param_value(node, "Alias"), NULL), node_child, "Parent");
    when_failed(retval, exit);

exit:
    amxc_string_clean(&child_alias);
    amxc_string_clean(&node_path);
    return retval;
}

int qos_dm_create_nodes_relations(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* nodes;

    when_null(qos, exit);

    nodes = qos_get_object(QOS_TR181_DEVICE_QOS_NODE_PATH);
    when_null_status(nodes, exit, retval = 0);

    amxd_object_for_each(instance, it, nodes) {
        amxd_object_t* node = amxc_container_of(it, amxd_object_t, it);
        amxd_object_t* ref = qos_get_object(GET_CHAR(amxd_object_get_param_value(node, "Reference"), NULL));
        amxc_string_t children;
        amxc_var_t child_list;

        when_null(ref, exit);
        amxc_string_init(&children, 0);
        amxc_var_init(&child_list);
        amxc_string_set(&children, GET_CHAR(amxd_object_get_param_value(ref, "Children"), NULL));
        retval = amxc_string_csv_to_var(&children, &child_list, NULL);
        when_failed(retval, exit);
        amxc_var_for_each(child, &child_list) {
            retval = qos_create_node_relations(node, amxc_var_constcast(cstring_t, child));
            when_failed(retval, exit);
        }
        amxc_string_clean(&children);
        amxc_var_clean(&child_list);
    }

    retval = 0;

exit:
    return retval;
}

//! [qos_dm_create_node]
int qos_dm_create_node(amxd_object_t* obj) {
    amxd_object_t* nodes_object;
    amxc_var_t params;
    amxc_var_t* tmp;
    amxc_string_t alias;
    char* obj_path;
    int retval = -1;

    nodes_object = qos_get_object(QOS_TR181_DEVICE_QOS_NODE_PATH);
    when_null_status(nodes_object, exit, retval = 0);

    amxc_var_init(&params);
    amxc_string_init(&alias, 0);
    amxc_var_set_type(&params, AMXC_VAR_ID_HTABLE);

    amxc_string_setf(&alias, "node-%s", GET_CHAR(amxd_object_get_param_value(obj, "Alias"), NULL));
    obj_path = amxd_object_get_path(obj, 0);

    tmp = amxc_var_add_new_key(&params, "Alias");
    amxc_var_push(cstring_t, tmp, amxc_string_take_buffer(&alias));
    amxc_var_add_key(cstring_t, &params, "Reference", obj_path);

    retval = qos_add_node_to_list(&params, nodes_object);

    free(obj_path);
    amxc_var_clean(&params);
    amxc_string_clean(&alias);
exit:
    return retval;
}
//! [qos_dm_create_node]
