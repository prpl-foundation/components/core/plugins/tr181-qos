/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <amxc/amxc.h>
#include <amxp/amxp_signal.h>

#include <amxd/amxd_action.h>

#include <netmodel/common_api.h>

#include "tr181-qos.h"
#include "dm-qos-shaper.h"
#include "qos-assert.h"
#include "qos-controller.h"

#include <stdio.h>

void _dm_qos_shaper_evt_instance_added(const char* const event_name UNUSED,
                                       const amxc_var_t* const data,
                                       void* const priv UNUSED) {

    amxd_object_t* shaper_object = NULL;
    amxd_object_t* shaper_instance = NULL;
    uint32_t index = 0;

    shaper_object = amxd_dm_signal_get_object(qos_get_dm(), data);
    index = GET_UINT32(data, "index");

    shaper_instance = amxd_object_get_instance(shaper_object, NULL, index);
    qos_controller_dm_shaper_added(shaper_instance);
}

void _dm_qos_shaper_evt_instance_changed(const char* const event_name UNUSED,
                                         const amxc_var_t* const data,
                                         void* const priv UNUSED) {

    amxd_object_t* shaper_instance = NULL;

    shaper_instance = amxd_dm_signal_get_object(qos_get_dm(), data);
    when_null(shaper_instance, exit);

    if(GETP_ARG(data, "parameters.Interface.to")) {
        qos_shaper_t* shaper = shaper_instance->priv;

        when_null(shaper, exit);
        netmodel_closeQuery(shaper->query);
        shaper->query = qos_controller_get_query(qos_shaper_interface_netdevname_changed, shaper, qos_shaper_dm_get_interface(shaper));
    } else {
        qos_controller_dm_shaper_changed(shaper_instance);
    }

exit:
    return;
}

amxd_status_t _dm_qos_shaper_action_on_delete(amxd_object_t* const shaper_object,
                                              amxd_param_t* const param,
                                              amxd_action_t reason,
                                              const amxc_var_t* const args,
                                              amxc_var_t* const action_retval,
                                              void* priv) {
    int retval = -1;
    uint32_t index = GET_UINT32(args, "index");
    amxd_object_t* const shaper_instance = amxd_object_get_instance(shaper_object, NULL, index);
    amxd_status_t status = amxd_status_ok;
    status = amxd_action_object_del_inst(shaper_object, param, reason, args, action_retval, priv);
    when_true(status != amxd_status_ok, exit);

    retval = qos_shaper_deactivate((qos_shaper_t*) shaper_instance->priv);
    if(retval != 0) {
        status = amxd_status_unknown_error;
    }

exit:
    return status;
}

amxd_status_t _dm_qos_shaper_action_on_destroy(amxd_object_t* const shaper_instance,
                                               amxd_param_t* const param,
                                               amxd_action_t reason,
                                               const amxc_var_t* const args,
                                               amxc_var_t* const action_retval,
                                               void* priv) {

    int retval = -1;
    amxd_status_t status = amxd_status_ok;

    status = amxd_action_object_destroy(shaper_instance, param, reason, args, action_retval, priv);
    when_true(status != amxd_status_ok, exit);

    retval = qos_controller_dm_shaper_removed(shaper_instance);
    when_failed(retval, exit);

    status = amxd_status_ok;

exit:
    return status;
}
