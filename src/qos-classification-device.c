/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <debug/sahtrace.h>

#include <fwrules/fw_folder.h>
#include <fwrules/fw.h>
#include <fwinterface/interface.h>

#include <gmap/gmap.h>

#include "qos-assert.h"
#include "qos.h"
#include "tr181-qos.h"
#include "qos-controller.h"
#include "qos-config.h"
#include "qos-firewall.h"
#include "qos-classification-device.h"

#define ME "class-device"

static const char* qos_classification_device_get_expr_operator_from_mode(const char* mode) {
    uint32_t i = 0;
    const char* modes[] = {
        "Exact", "Prefix", "Suffix", "Substring", NULL
    };
    const char* results[] = {
        "==", "starts with", "ends with", "matches", NULL
    };

    while((modes[i] != NULL) && (results[i] != NULL)) {
        if(strncmp(mode, modes[i], strlen(modes[i])) == 0) {
            return results[i];
        }
        i++;
    }
    return NULL;
}

/*
 * Builds a gmap expression to filter devices based on the VendorClassID parameter. The expression
 * will look as follows:
 *  [!](.VendorClassID <MODE> '<SourceVendorClassIDValue>')
 *
 * The starting exclamation mark is optional and depends on SourceVendorClassIDExclude, the <MODE>
 * will be one of '==', 'starts with', 'ends with' or 'matches'. This depends on the value of
 * SourceVendorClassIDMode. The SourceVendorClassIDValue is the value of the SourceVendorClassID
 * parameter.
 *
 * The same comment applies to the qos_classification_device_build_expr_vendor_class_id_dst
 * function, but with Source replaced by Dest.
 */
static void qos_classification_device_build_expr_vendor_class_id_src(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = NULL;
    const char* mode = NULL;
    const char* mode_oper = NULL;
    bool exclude = false;

    id = qos_classification_dm_get_source_vendor_class_id(classification);
    when_str_empty(id, exit);

    mode = qos_classification_dm_get_source_vendor_class_id_mode(classification);
    when_str_empty(mode, exit);

    mode_oper = qos_classification_device_get_expr_operator_from_mode(mode);
    exclude = qos_classification_dm_get_source_vendor_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s(.VendorClassID %s '%s')", exclude ? "!" : "", mode_oper, id);

exit:
    return;
}

/*
 * See qos_classification_device_build_expr_vendor_class_id_src for documentation.
 */
static void qos_classification_device_build_expr_vendor_class_id_dst(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = NULL;
    const char* mode = NULL;
    const char* mode_oper = NULL;
    bool exclude = false;

    id = qos_classification_dm_get_dest_vendor_class_id(classification);
    when_str_empty(id, exit);

    mode = qos_classification_dm_get_dest_vendor_class_id_mode(classification);
    when_str_empty(mode, exit);

    mode_oper = qos_classification_device_get_expr_operator_from_mode(mode);
    exclude = qos_classification_dm_get_dest_vendor_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s(.VendorClassID %s '%s')", exclude ? "!" : "", mode_oper, id);

exit:
    return;
}

static void qos_classification_device_build_expr_vendor_class_id_v6_src(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = qos_classification_dm_get_source_vendor_class_id_v6(classification);

    when_str_empty(id, exit);

    bool exclude = qos_classification_dm_get_source_vendor_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s(.VendorClassIDv6 == '%s')", exclude ? "!" : "", id);

exit:
    return;
}


static void qos_classification_device_build_expr_vendor_class_id_v6_dst(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = qos_classification_dm_get_dest_vendor_class_id_v6(classification);

    when_str_empty(id, exit);

    bool exclude = qos_classification_dm_get_dest_vendor_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s(.VendorClassIDv6 == '%s')", exclude ? "!" : "", id);

exit:
    return;
}

/*
 * Builds a gmap expression to filter devices based on the UserClassID parameter. It is possible
 * that the expression already contains a part from the VendorClassID family of parameters. If this
 * is the case, the expression will be prefixed with ' && '.
 *
 * The expression will look as follows:
 *  [ && ][!](.UserClassID == '<SourceUserClassIDValue>')
 *
 * The starting exclamation mark is optional and depends on SourceUserClassIDExclude.
 * The SourceUserClassIDValue is the value of the SourceUserClassID parameter.
 *
 * The same comment applies to the qos_classification_device_build_expr_user_class_id_dst
 * function, but with Source replaced by Dest.
 */
static void qos_classification_device_build_expr_user_class_id_src(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = qos_classification_dm_get_source_user_class_id(classification);
    bool expr_is_empty = amxc_string_is_empty(expr);

    when_str_empty(id, exit);

    bool exclude = qos_classification_dm_get_source_user_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s%s(.UserClassID == '%s')",
                        expr_is_empty ? "" : " && ",
                        exclude ? "!" : "",
                        id);

exit:
    return;
}

static void qos_classification_device_build_expr_user_class_id_dst(amxc_string_t* expr, const qos_classification_t* const classification) {
    const char* id = qos_classification_dm_get_dest_user_class_id(classification);
    bool expr_is_empty = amxc_string_is_empty(expr);

    when_str_empty(id, exit);

    bool exclude = qos_classification_dm_get_dest_user_class_id_exclude(classification);
    amxc_string_appendf(expr, "%s%s(.UserClassID == '%s')",
                        expr_is_empty ? "" : " && ",
                        exclude ? "!" : "",
                        id);

exit:
    return;
}

static void qos_classification_device_build_expression_src(amxc_string_t* expr, const qos_classification_t* const classification) {

    if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
        qos_classification_device_build_expr_vendor_class_id_src(expr, classification);
    } else {
        qos_classification_device_build_expr_vendor_class_id_v6_src(expr, classification);
    }
    qos_classification_device_build_expr_user_class_id_src(expr, classification);

    return;
}

static void qos_classification_device_build_expression_dst(amxc_string_t* expr, const qos_classification_t* const classification) {

    if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
        qos_classification_device_build_expr_vendor_class_id_dst(expr, classification);
    } else {
        qos_classification_device_build_expr_vendor_class_id_v6_dst(expr, classification);
    }
    qos_classification_device_build_expr_user_class_id_dst(expr, classification);

    return;
}

/*
   gmap returns a list of IP addresses in the following format
   ```
    IPv4Address = [
        {
            Address = "172.18.0.2",
            AddressSource = "",
            Reserved = 0,
            Scope = "unknown",
            Status = "not reachable"
        }
    ]
   ```
 */
static amxc_var_t* qos_classification_device_get_ips(const qos_classification_t* const classification, amxc_var_t* device_params) {
    amxc_var_t* result = NULL;

    if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
        result = GET_ARG(device_params, "IPv4Address");
    } else {
        result = GET_ARG(device_params, "IPv6Address");
    }

    return result;
}

static int qos_classification_device_activate_source(const qos_classification_t* const classification, const char* src_ip) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_SOURCE);
    retval |= fw_rule_set_source(rule, src_ip);
    retval |= fw_folder_push_rule(folder, rule);

exit:
    return retval;
}

static int qos_classification_device_activate_dest(const qos_classification_t* const classification, const char* dst_ip) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_DESTINATION);
    retval |= fw_rule_set_destination(rule, dst_ip);
    retval |= fw_folder_push_rule(folder, rule);

exit:
    return retval;
}

static int qos_classification_device_ip_addrs_add(const qos_classification_t* const classification,
                                                  const char* alias,
                                                  amxc_var_t* device_params,
                                                  bool src) {
    int retval = -1;
    amxc_var_t* ip_addrs = NULL;
    qos_classification_device_t* class_device = NULL;
    amxc_var_t* device_rules = NULL;
    amxc_var_t* ip_list = NULL;

    when_null(classification, exit);
    when_str_empty(alias, exit);
    when_null(device_params, exit);

    class_device = qos_classification_device_get_class_device(classification);
    when_null(class_device, exit);

    ip_addrs = qos_classification_device_get_ips(classification, device_params);
    when_null(ip_addrs, exit);
    when_true_status(amxc_llist_is_empty(amxc_var_constcast(amxc_llist_t, ip_addrs)), exit, retval = 0);

    if(src) {
        device_rules = qos_classification_device_get_device_rules_src(class_device);
    } else {
        device_rules = qos_classification_device_get_device_rules_dst(class_device);
    }
    when_null(device_rules, exit);

    ip_list = amxc_var_add_key(amxc_llist_t, device_rules, alias, NULL);
    amxc_var_for_each(ip_addr, ip_addrs) {
        amxc_var_add(cstring_t, ip_list, GET_CHAR(ip_addr, "Address"));
    }

    retval = 0;

exit:
    return retval;
}

static int qos_classification_device_ip_addrs_del(qos_classification_t* classification,
                                                  const char* alias,
                                                  bool src) {
    int retval = -1;
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);
    amxc_var_t* device_rules = NULL;
    amxc_var_t* ip_list = NULL;

    when_str_empty(alias, exit);

    if(src) {
        device_rules = qos_classification_device_get_device_rules_src(class_device);
    } else {
        device_rules = qos_classification_device_get_device_rules_dst(class_device);
    }
    when_null(device_rules, exit);

    ip_list = GET_ARG(device_rules, alias);
    amxc_var_delete(&ip_list);
    retval = 0;

exit:
    return retval;
}

static void qos_classification_device_trigger_signal(const qos_classification_t* classification,
                                                     const char* alias,
                                                     const amxc_var_t* const device_params,
                                                     gmap_query_action_t action) {
    int32_t pmark = 0;
    const char* phys_addr = NULL;
    amxc_var_t data;

    amxc_var_init(&data);

    when_null(classification, exit);
    when_str_empty(alias, exit);

    amxc_var_set_type(&data, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, &data, "action", action);
    amxc_var_add_key(cstring_t, &data, "alias", alias);

    phys_addr = GET_CHAR(device_params, "PhysAddress");
    if(phys_addr != NULL) {
        amxc_var_add_key(cstring_t, &data, "PhysAddress", phys_addr);
    }

    if(classification->class_intf->netdev_output_interface != NULL) {
        amxc_var_add_key(cstring_t, &data, "NetDevName", classification->class_intf->netdev_output_interface);
    }

    if(qos_dm_get_broken_qdisc_priomap()) {
        amxc_var_add_key(bool, &data, "BrokenQDiscPrioMap", true);
    }

    pmark = qos_classification_dm_get_ethernet_priority_mark(classification);
    amxc_var_add_key(int32_t, &data, "EthernetPriorityMark", pmark);

    amxp_sigmngr_trigger_signal(NULL, SIG_DEVICE_UPDATED, &data);

exit:
    amxc_var_clean(&data);
    return;
}

static int qos_classification_device_activate_source_all_ips(const qos_classification_t* classification, amxc_var_t* device) {
    int retval = 0;

    amxc_var_for_each(ip, device) {
        retval |= qos_classification_device_activate_source(classification, amxc_var_constcast(cstring_t, ip));
    }

    return retval;
}

static int qos_classification_device_activate_source_all_devices(const qos_classification_t* const classification) {
    int retval = -1;
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);
    amxc_var_t* device_rules = NULL;

    when_null(class_device, exit);

    device_rules = qos_classification_device_get_device_rules_src(class_device);
    when_null(device_rules, exit);

    // Return 0 if there is nothing to activate for any device
    when_true_status(GETI_ARG(device_rules, 0) == NULL, exit, retval = 0);

    retval = 0;
    amxc_var_for_each(device, device_rules) {
        retval |= qos_classification_device_activate_source_all_ips(classification, device);
    }

exit:
    return retval;
}

static int qos_classification_device_activate_dest_all_ips(const qos_classification_t* classification, amxc_var_t* device) {
    int retval = 0;

    amxc_var_for_each(ip, device) {
        retval |= qos_classification_device_activate_dest(classification, amxc_var_constcast(cstring_t, ip));
    }

    return retval;
}

static int qos_classification_device_activate_dest_all_devices(const qos_classification_t* const classification) {
    int retval = -1;
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);
    amxc_var_t* device_rules = NULL;

    when_null(class_device, exit);

    device_rules = qos_classification_device_get_device_rules_dst(class_device);
    when_null(device_rules, exit);

    // Return 0 if there is nothing to activate for any device
    when_true_status(GETI_ARG(device_rules, 0) == NULL, exit, retval = 0);

    retval = 0;
    amxc_var_for_each(device, device_rules) {
        retval |= qos_classification_device_activate_dest_all_ips(classification, device);
    }

exit:
    return retval;
}

static int qos_classification_device_reset_src(qos_classification_t* classification, const char* alias, amxc_var_t* device_params) {
    int retval = -1;

    when_null(classification, exit);
    when_str_empty(alias, exit);

    // Start from fresh folder
    retval = qos_classification_reset_folders(classification);
    when_failed(retval, exit);

    // Delete IP addresses for current device
    retval = qos_classification_device_ip_addrs_del(classification, alias, true);
    when_failed(retval, exit);

    // Add IP addresses for current device
    // This is allowed to fail if there are no IPs to add
    (void) qos_classification_device_ip_addrs_add(classification, alias, device_params, true);

    // Activate default rules
    retval = qos_classification_activate(classification);
    when_failed(retval, exit);

    // Activate IP rules for discovered devices
    retval = qos_classification_device_activate_source_all_devices(classification);
    when_failed(retval, exit);
    retval = qos_classification_device_activate_dest_all_devices(classification);
    when_failed(retval, exit);

exit:
    return retval;
}

static void qos_classification_device_cb_src(gmap_query_t* query, const char* alias,
                                             amxc_var_t* device_params, gmap_query_action_t action) {
    qos_classification_t* classification = NULL;
    int retval = -1;

    when_null(query, exit);

    SAH_TRACEZ_INFO(ME, "Found device %s with matching ID and action %d", alias, action);
    classification = (qos_classification_t*) query->data;
    when_null(classification, exit);

    switch(action) {
    case gmap_query_expression_start_matching:
        (void) qos_classification_device_ip_addrs_add(classification, alias, device_params, true);
        retval = qos_classification_device_activate_source_all_devices(classification);
        break;
    case gmap_query_device_updated:
        retval = qos_classification_device_reset_src(classification, alias, device_params);
        break;
    case gmap_query_expression_stop_matching:
        retval = qos_classification_device_reset_src(classification, alias, device_params);
        break;
    case gmap_query_error:
        SAH_TRACEZ_ERROR(ME, "Something went wrong with gmap query for device [%s]", alias);
    default:
        break;
    }

    when_failed(retval, exit);
    qos_classification_device_trigger_signal(classification, alias, device_params, action);

    fw_folder_set_enabled(qos_classification_get_folder(classification), true);
    fw_commit(fw_rule_callback);
    retval = fw_apply();

exit:
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }
    return;
}

static int qos_classification_device_reset_dst(qos_classification_t* classification, const char* alias, amxc_var_t* device_params) {
    int retval = -1;

    when_null(classification, exit);
    when_str_empty(alias, exit);

    // Start from fresh folder
    retval = qos_classification_reset_folders(classification);
    when_failed(retval, exit);

    // Delete IP addresses for current device
    retval = qos_classification_device_ip_addrs_del(classification, alias, false);
    when_failed(retval, exit);

    // Add IP addresses for current device
    // This is allowed to fail if there are no IPs to add
    (void) qos_classification_device_ip_addrs_add(classification, alias, device_params, false);

    // Activate default rules
    retval = qos_classification_activate(classification);
    when_failed(retval, exit);

    // Activate IP rules for discovered devices
    retval = qos_classification_device_activate_source_all_devices(classification);
    when_failed(retval, exit);
    retval = qos_classification_device_activate_dest_all_devices(classification);
    when_failed(retval, exit);

exit:
    return retval;
}

static void qos_classification_device_cb_dst(gmap_query_t* query, const char* alias,
                                             amxc_var_t* device_params, gmap_query_action_t action) {
    qos_classification_t* classification = NULL;
    int retval = -1;

    when_null(query, exit);

    SAH_TRACEZ_INFO(ME, "Found device %s with matching ID and action %d", alias, action);
    classification = (qos_classification_t*) query->data;
    when_null(classification, exit);

    switch(action) {
    case gmap_query_expression_start_matching:
        (void) qos_classification_device_ip_addrs_add(classification, alias, device_params, false);
        retval = qos_classification_device_activate_dest_all_devices(classification);
        break;
    case gmap_query_device_updated:
        retval = qos_classification_device_reset_dst(classification, alias, device_params);
        break;
    case gmap_query_expression_stop_matching:
        retval = qos_classification_device_reset_dst(classification, alias, device_params);
        break;
    case gmap_query_error:
        SAH_TRACEZ_ERROR(ME, "Something went wrong with gmap query for device [%s]", alias);
    default:
        break;
    }

    when_failed(retval, exit);
    qos_classification_device_trigger_signal(classification, alias, device_params, action);

    fw_folder_set_enabled(qos_classification_get_folder(classification), true);
    fw_commit(fw_rule_callback);
    retval = fw_apply();

exit:
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }
    return;
}

/*
   Get a unique name when opening a query.

   The returned data must be freed by the caller.
 */
static char* qos_classification_device_get_query_name(void) {
    static uint32_t counter = 0;
    char* retval = NULL;
    amxc_string_t name;

    amxc_string_init(&name, 0);
    amxc_string_setf(&name, "%s-%d", ME, counter);
    counter++;

    retval = amxc_string_take_buffer(&name);
    amxc_string_clean(&name);
    return retval;
}

static int qos_classification_device_open_query_src(const qos_classification_t* const classification, const char* expr) {
    int retval = -1;
    char* name = qos_classification_device_get_query_name();
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);

    SAH_TRACEZ_INFO(ME, "Opening gmap query with expression %s", expr);
    class_device->query_src = gmap_query_open_ext(expr, name, qos_classification_device_cb_src, (void*) classification);
    if(class_device->query_src == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open gmap query with expression %s", expr);
        goto exit;
    }

    retval = 0;

exit:
    free(name);
    return retval;
}

static int qos_classification_device_based_activate_src(const qos_classification_t* const classification) {
    int retval = -1;
    amxc_string_t expr;
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);

    amxc_string_init(&expr, 0);
    when_null(class_device, exit);

    qos_classification_device_build_expression_src(&expr, classification);
    when_str_empty_status(amxc_string_get(&expr, 0), exit, retval = 0);

    // Set required feature now, because folder should not be activated before we reach a gmap callback
    retval = fw_folder_set_feature(qos_classification_get_folder(classification), FW_FEATURE_SOURCE);
    when_failed(retval, exit);

    when_not_null(class_device->query_src, exit);
    retval = qos_classification_device_open_query_src(classification, amxc_string_get(&expr, 0));

exit:
    amxc_string_clean(&expr);
    return retval;
}

static int qos_classification_device_open_query_dst(const qos_classification_t* const classification, const char* expr) {
    int retval = -1;
    char* name = qos_classification_device_get_query_name();
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);

    SAH_TRACEZ_INFO(ME, "Opening gmap query with expression %s", expr);
    class_device->query_dst = gmap_query_open_ext(expr, name, qos_classification_device_cb_dst, (void*) classification);
    if(class_device->query_dst == NULL) {
        SAH_TRACEZ_ERROR(ME, "Failed to open gmap query with expression %s", expr);
        goto exit;
    }

    retval = 0;

exit:
    free(name);
    return retval;
}

static int qos_classification_device_based_activate_dst(const qos_classification_t* const classification) {
    int retval = -1;
    amxc_string_t expr;
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);

    amxc_string_init(&expr, 0);
    when_null(class_device, exit);

    qos_classification_device_build_expression_dst(&expr, classification);
    when_str_empty_status(amxc_string_get(&expr, 0), exit, retval = 0);

    // Set required feature now, because folder should not be activated before we reach a gmap callback
    retval = fw_folder_set_feature(qos_classification_get_folder(classification), FW_FEATURE_DESTINATION);
    when_failed(retval, exit);

    when_not_null(class_device->query_dst, exit);
    retval = qos_classification_device_open_query_dst(classification, amxc_string_get(&expr, 0));

exit:
    amxc_string_clean(&expr);
    return retval;
}

static int qos_classification_device_set_class_device(const qos_classification_t* const classification, qos_classification_device_t* class_device) {
    int retval = -1;

    when_null(classification, exit);
    when_null(class_device, exit);

    ((qos_classification_t* const) classification)->class_device = class_device;
    retval = 0;

exit:
    return retval;
}

static int qos_classification_device_new(qos_classification_device_t** class_device, const qos_classification_t* const classification) {
    int retval = -1;

    when_null(class_device, exit);
    when_not_null_status(classification->class_device, exit, retval = 0);
    when_null(classification, exit);

    *class_device = (qos_classification_device_t*) calloc(1, sizeof(qos_classification_device_t));
    when_null(*class_device, exit);

    retval = qos_classification_device_set_class_device(classification, *class_device);
    if(retval != 0) {
        free(*class_device);
        goto exit;
    }

    amxc_var_init(&(*class_device)->device_rules_src);
    amxc_var_set_type(&(*class_device)->device_rules_src, AMXC_VAR_ID_HTABLE);
    amxc_var_init(&(*class_device)->device_rules_dst);
    amxc_var_set_type(&(*class_device)->device_rules_dst, AMXC_VAR_ID_HTABLE);

    retval = 0;
exit:
    return retval;
}

static void qos_classification_device_delete(const qos_classification_t* const classification) {
    qos_classification_device_t* class_device = NULL;

    when_null(classification, exit);

    class_device = classification->class_device;
    when_null(class_device, exit);

    gmap_query_close(class_device->query_src);
    gmap_query_close(class_device->query_dst);

    amxc_var_clean(&class_device->device_rules_src);
    amxc_var_clean(&class_device->device_rules_dst);
    free(class_device);
    ((qos_classification_t*) classification)->class_device = NULL;

exit:
    return;
}

int qos_classification_device_based_activate(const qos_classification_t* const classification) {
    int retval = 0;
    qos_classification_device_t* class_device = NULL;

    retval = qos_classification_device_new(&class_device, classification);
    when_failed(retval, exit);

    // Open gmap query to find devices with a matching source VendorClassID, UserClassID
    retval |= qos_classification_device_based_activate_src(classification);
    // Open gmap query to find devices with a matching destination VendorClassID, UserClassID
    retval |= qos_classification_device_based_activate_dst(classification);

exit:
    return retval;
}

void qos_classification_device_based_deactivate(const qos_classification_t* const classification) {
    qos_classification_device_t* class_device = qos_classification_device_get_class_device(classification);

    when_null(class_device, exit);

    // Clean up data for mod-qos-vlan0-tag
    amxc_var_for_each(device, &class_device->device_rules_src) {
        qos_classification_device_trigger_signal(classification, amxc_var_key(device), NULL, gmap_query_expression_stop_matching);
    }
    amxc_var_for_each(device, &class_device->device_rules_dst) {
        qos_classification_device_trigger_signal(classification, amxc_var_key(device), NULL, gmap_query_expression_stop_matching);
    }
    qos_classification_device_delete(classification);

exit:
    return;
}

int _qos_classification_device_entrypoint(int reason,
                                          UNUSED amxd_dm_t* dm,
                                          UNUSED amxo_parser_t* parser) {
    switch(reason) {
    case 0:     // START
        SAH_TRACEZ_WARNING(ME, "Initializing gmap");
        gmap_client_init(amxb_be_who_has("QoS."));
        break;
    }

    return 0;
}
