/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <netmodel/common_api.h>

#include "qos-assert.h"
#include "qos.h"
#include "tr181-qos.h"
#include "qos-controller.h"
#include "qos-config.h"
#include "qos-firewall.h"

#if defined(DEVICE_BASED_QOS)
#include "qos-classification-device.h"
#endif

#include <debug/sahtrace.h>

#define ME "class"

static int qos_classification_set_query_input_interface(const qos_classification_t* const classification, netmodel_query_t* query);
static int qos_classification_set_query_output_interface(const qos_classification_t* const classification, netmodel_query_t* query);
static int qos_classification_set_query_bridge_input_interface(const qos_classification_t* const classification, netmodel_query_t* query);
static int qos_classification_set_query_bridge_output_interface(const qos_classification_t* const classification, netmodel_query_t* query);

int qos_classification_delete(qos_classification_t** classification) {
    int retval = -1;

    when_null(classification, exit);
    when_null(*classification, exit);

    //first deactivate the classification
    qos_classification_deactivate(*classification);

    //cleanup internals
    qos_classification_deinit(*classification);

    free(*classification);
    *classification = NULL;
    retval = 0;

exit:
    return retval;
}

static int qos_classification_input_interface_init(qos_classification_t* const classification) {
    int retval = -1;
    const char* interface = NULL;
    netmodel_query_t* query = NULL;

    when_null(classification, exit);

    retval = 0;

    interface = qos_classification_dm_get_interface(classification);
    when_str_empty(interface, exit);

    query = netmodel_openQuery_getFirstParameter(interface, qos_config_get_name(), "NetDevName", "netdev-bound", "down",
                                                 qos_classification_input_interface_netdevname_changed, (qos_classification_t*) classification);
    when_null_status(query, exit, retval = -1);

    retval = qos_classification_set_query_input_interface(classification, query);

exit:
    return retval;
}

static int qos_classification_output_interface_init(qos_classification_t* const classification) {
    int retval = -1;
    const char* interface = NULL;
    netmodel_query_t* query = NULL;

    when_null(classification, exit);

    retval = 0;

    interface = qos_classification_dm_get_output_interface(classification);
    when_str_empty(interface, exit);

    query = netmodel_openQuery_getFirstParameter(interface, qos_config_get_name(), "NetDevName", "netdev-bound", "down",
                                                 qos_classification_output_interface_netdevname_changed, (qos_classification_t*) classification);
    when_null_status(query, exit, retval = -1);

    retval = qos_classification_set_query_output_interface(classification, query);

exit:
    return retval;
}

static int qos_classification_bridge_input_interface_init(qos_classification_t* const classification) {
    int retval = -1;
    const char* interface = NULL;
    netmodel_query_t* query = NULL;

    when_null(classification, exit);

    retval = 0;

    interface = qos_classification_dm_get_bridge_input_interface(classification);
    when_str_empty(interface, exit);

    query = netmodel_openQuery_getFirstParameter(interface, qos_config_get_name(), "NetDevName", "netdev-bound", "down",
                                                 qos_classification_bridge_input_interface_netdevname_changed, (qos_classification_t*) classification);
    when_null_status(query, exit, retval = -1);

    retval = qos_classification_set_query_bridge_input_interface(classification, query);

exit:
    return retval;
}

static int qos_classification_bridge_output_interface_init(qos_classification_t* const classification) {
    int retval = -1;
    const char* interface = NULL;
    netmodel_query_t* query = NULL;

    when_null(classification, exit);

    retval = 0;

    interface = qos_classification_dm_get_bridge_output_interface(classification);
    when_str_empty(interface, exit);

    query = netmodel_openQuery_getFirstParameter(interface, qos_config_get_name(), "NetDevName", "netdev-bound", "down",
                                                 qos_classification_bridge_output_interface_netdevname_changed, (qos_classification_t*) classification);
    when_null_status(query, exit, retval = -1);

    retval = qos_classification_set_query_bridge_output_interface(classification, query);

exit:
    return retval;
}

static int qos_classification_interface_init(qos_classification_t* const classification) {
    int retval = 0;
    retval |= qos_classification_input_interface_init(classification);
    retval |= qos_classification_output_interface_init(classification);
    retval |= qos_classification_bridge_input_interface_init(classification);
    retval |= qos_classification_bridge_output_interface_init(classification);
    return retval;
}

static int qos_classification_interface_new(qos_classification_interface_t** const class_intf) {
    int retval = -1;

    when_null(class_intf, exit);
    when_not_null(*class_intf, exit);

    *class_intf = (qos_classification_interface_t*) calloc(1, sizeof(qos_classification_interface_t));
    when_null(*class_intf, exit);

    retval = 0;

exit:
    return retval;
}

int qos_classification_init(qos_classification_t* const classification, amxd_object_t* const classification_instance) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification_instance, exit);

    memset(classification, 0, sizeof(qos_classification_t));
    classification_instance->priv = (void*) classification;
    classification->dm_object = classification_instance;
    classification->ip_based = true;

    when_failed(fw_folder_new(&classification->folder4), exit);
    when_failed(fw_folder_new(&classification->folder6), exit);
    when_failed(qos_classification_interface_new(&classification->class_intf), exit);
    when_failed(qos_classification_interface_new(&classification->class_bridge_intf), exit);
    when_failed(qos_classification_interface_init(classification), exit);

    retval = 0;

exit:
    return retval;
}

static int qos_classification_interface_delete(qos_classification_interface_t** const class_intf) {
    int retval = -1;

    when_null(class_intf, exit);
    when_null(*class_intf, exit);

    netmodel_closeQuery((*class_intf)->query_input_interface);
    netmodel_closeQuery((*class_intf)->query_output_interface);
    free((*class_intf)->netdev_input_interface);
    free((*class_intf)->netdev_output_interface);

    free(*class_intf);
    *class_intf = NULL;
    retval = 0;

exit:
    return retval;
}

int qos_classification_deinit(qos_classification_t* const classification) {
    int retval = -1;

    when_null(classification, exit);

    qos_classification_interface_delete(&classification->class_intf);
    qos_classification_interface_delete(&classification->class_bridge_intf);
    classification->dm_object->priv = NULL;
    fw_folder_delete(&classification->folder4);
    fw_folder_delete(&classification->folder6);
    fw_commit(fw_rule_callback);

    retval = 0;

exit:
    return retval;
}

// IP based classification is done based on IPVersion, device based classification is done based on DHCPType.
fw_folder_t* qos_classification_get_folder(const qos_classification_t* const classification) {
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    if(classification->ip_based) {
        if(qos_classification_dm_get_ip_version(classification) == 6) {
            folder = classification->folder6;
        } else {
            folder = classification->folder4;
        }
    } else {
        if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
            folder = classification->folder4;
        } else {
            folder = classification->folder6;
        }
    }

exit:
    return folder;
}

bool fw_rule_callback(const fw_rule_t* const rule, const fw_rule_flag_t flag,
                      UNUSED const char* chain, UNUSED const char* table, int index) {
    int ret = false;
    bool enabled = fw_rule_is_enabled(rule);
    const char* str_flag = "Unknown";

    switch(flag) {
    case FW_RULE_FLAG_NEW:
        str_flag = "new";
        break;
    case FW_RULE_FLAG_MODIFIED:
        str_flag = "modified";
        break;
    case FW_RULE_FLAG_DELETED:
        str_flag = "deleted";
        break;
    case FW_RULE_FLAG_HANDLED:
        str_flag = "handled";
        break;
    case FW_RULE_FLAG_LAST:
        str_flag = "last (nothing)";
        break;
    }

    SAH_TRACEZ_NOTICE(ME, "Event rule: flag[%s] chain[%s] table[%s] index[%d] enabled[%s]",
                      str_flag, chain, table, index, enabled ? "true" : "false");

    if((flag != FW_RULE_FLAG_DELETED) && enabled) {
        ret = fw_replace_rule(rule, index);
    } else {
        ret = fw_delete_rule(rule, index);
    }

    return (ret == 0);
}

static int qos_classification_activate_set_chain(const qos_classification_t* const classification,
                                                 fw_rule_t* rule) {

    int retval = -1;
    const char* direction = NULL;
    const char* chain_target = NULL;
    const char* chain = NULL;
    bool ipv4 = true;
    uint32_t i = 0;
    const char* directions[] = {
        "Prerouting", "Input", "Forward", "Output", "Postrouting", NULL
    };
    qos_config_iptables_target targets[] = {
        { QOS_CONFIG_IPTABLES_PREROUTING_TARGET, QOS_CONFIG_IP6TABLES_PREROUTING_TARGET },
        { QOS_CONFIG_IPTABLES_INPUT_TARGET, QOS_CONFIG_IP6TABLES_INPUT_TARGET },
        { QOS_CONFIG_IPTABLES_FORWARD_TARGET, QOS_CONFIG_IP6TABLES_FORWARD_TARGET },
        { QOS_CONFIG_IPTABLES_OUTPUT_TARGET, QOS_CONFIG_IP6TABLES_OUTPUT_TARGET },
        { QOS_CONFIG_IPTABLES_POSTROUTING_TARGET, QOS_CONFIG_IP6TABLES_POSTROUTING_TARGET },
        { NULL, NULL },
    };

    when_null(classification, exit);
    when_null(rule, exit);

    ipv4 = !(qos_classification_dm_get_ip_version(classification) == 6);

    direction = qos_classification_dm_get_direction(classification);
    when_str_empty(direction, exit);

    while((directions[i] != NULL) && (targets[i].ipv4_target != NULL)) {
        if(strncmp(direction, directions[i], strlen(directions[i])) == 0) {
            chain_target = ipv4 ? targets[i].ipv4_target : targets[i].ipv6_target;
        }
        i++;
    }
    when_str_empty(chain_target, exit);

    chain = (ipv4 ? qos_config_get_iptables_target(chain_target) : qos_config_get_ip6tables_target(chain_target));
    when_null(chain, exit);

    retval = 0;

    retval |= fw_rule_set_chain(rule, chain);

exit:
    return retval;
}

static int qos_classification_activate_set_destination(const qos_classification_t* const classification) {
    int retval = -1;
    const char* dest_ip = NULL;
    const char* dest_mask = NULL;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);

    retval = 0;
    dest_ip = qos_classification_dm_get_dest_ip(classification);
    when_null(dest_ip, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    rule = fw_folder_fetch_default_rule(folder);
    when_null(rule, exit);

    retval |= fw_rule_set_destination(rule, dest_ip);
    retval |= fw_rule_set_destination_excluded(rule,
                                               qos_classification_dm_get_dest_ip_exclude(classification));

    dest_mask = qos_classification_dm_get_dest_mask(classification);
    if(dest_mask) {
        retval |= fw_rule_set_destination_mask(rule, dest_mask);
    } else if(qos_classification_dm_get_ip_version(classification) != 6) {
        retval |= fw_rule_set_destination_mask(rule, "255.255.255.255");
    }

    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

exit:
    return retval;
}

static int qos_classification_activate_set_source(const qos_classification_t* const classification) {
    int retval = -1;
    const char* source_ip = NULL;
    const char* source_mask = NULL;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);

    retval = 0;
    source_ip = qos_classification_dm_get_source_ip(classification);
    when_null(source_ip, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    rule = fw_folder_fetch_default_rule(folder);
    when_null(rule, exit);

    retval |= fw_rule_set_source(rule, source_ip);
    retval |= fw_rule_set_source_excluded(rule, qos_classification_dm_get_source_ip_exclude(classification));

    source_mask = qos_classification_dm_get_source_mask(classification);
    if(source_mask) {
        retval |= fw_rule_set_source_mask(rule, source_mask);
    } else if(qos_classification_dm_get_ip_version(classification) != 6) {
        retval |= fw_rule_set_source_mask(rule, "255.255.255.255");
    }

    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

exit:
    return retval;
}

static int qos_classification_activate_set_protocol(const qos_classification_t* const classification,
                                                    fw_rule_t* rule) {

    int retval = -1;
    int protocol = -1;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    protocol = qos_classification_dm_get_protocol(classification);

    if(protocol >= 0) {
        retval |= fw_rule_set_protocol(rule, (uint32_t) protocol);
        retval |= fw_rule_set_protocol_excluded(rule,
                                                qos_classification_dm_get_protocol_exclude(classification));
    }

exit:
    return retval;
}

static int qos_classification_activate_set_destination_port(const qos_classification_t* const classification,
                                                            fw_rule_t* rule) {

    int retval = -1;
    int32_t destination_port = -1;
    int32_t destination_port_range_max = -1;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    destination_port = qos_classification_dm_get_dest_port(classification);

    if(destination_port >= 0) {
        retval |= fw_rule_set_destination_port(rule, (uint32_t) destination_port);
        retval |= fw_rule_set_destination_port_excluded(rule,
                                                        qos_classification_dm_get_dest_port_exclude(classification));

        destination_port_range_max = qos_classification_dm_get_dest_port_range_max(classification);
        if(destination_port_range_max >= 0) {
            retval |= fw_rule_set_destination_port_range_max(rule, (uint32_t) destination_port_range_max);
        }
    }

exit:
    return retval;
}

static int qos_classification_activate_set_source_port(const qos_classification_t* const classification,
                                                       fw_rule_t* rule) {

    int retval = -1;
    int32_t source_port = -1;
    int32_t source_port_range_max = -1;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    source_port = qos_classification_dm_get_source_port(classification);

    if(source_port >= 0) {
        retval |= fw_rule_set_source_port(rule, (uint32_t) source_port);
        retval |= fw_rule_set_source_port_excluded(rule,
                                                   qos_classification_dm_get_source_port_exclude(classification));

        source_port_range_max = qos_classification_dm_get_source_port_range_max(classification);
        if(source_port_range_max >= 0) {
            retval |= fw_rule_set_source_port_range_max(rule, (uint32_t) source_port_range_max);
        }
    }

exit:
    return retval;
}

static int qos_classification_activate_set_source_mac(const qos_classification_t* const classification,
                                                      fw_rule_t* rule) {

    int retval = -1;
    const char* source_mac = NULL;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    source_mac = qos_classification_dm_get_source_mac_address(classification);
    when_null(source_mac, exit);

    retval |= fw_rule_set_source_mac_address(rule, source_mac);
    retval |= fw_rule_set_source_mac_excluded(rule,
                                              qos_classification_dm_get_source_mac_exclude(classification));

exit:
    return retval;
}

static int qos_classification_activate_set_destination_mac(const qos_classification_t* const classification,
                                                           fw_rule_t* rule) {

    int retval = -1;
    const char* dest_mac = NULL;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    dest_mac = qos_classification_dm_get_dest_mac_address(classification);
    when_null(dest_mac, exit);

    //retval |= fw_rule_set_destination_mac_address(rule, dest_mac);
    retval |= fw_rule_set_destination_mac_excluded(rule,
                                                   qos_classification_dm_get_dest_mac_exclude(classification));

exit:
    return retval;
}

static int qos_classification_activate_set_dscp_check(const qos_classification_t* const classification,
                                                      fw_rule_t* rule) {

    int retval = -1;
    int32_t dscp_check = -1;

    when_null(classification, exit);
    when_null(rule, exit);

    retval = 0;

    dscp_check = qos_classification_dm_get_dscp_check(classification);

    if(dscp_check >= 0) {
        retval |= fw_rule_set_dscp(rule, (uint32_t) dscp_check);
        retval |= fw_rule_set_dscp_excluded(rule, qos_classification_dm_get_dscp_exclude(classification));
    }

exit:
    return retval;
}

static int qos_classification_activate_set_default_rule(const qos_classification_t* const classification) {
    int retval = -1;
    fw_folder_t* folder = NULL;
    fw_rule_t* rule = NULL;
    bool ipv4 = true;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    rule = fw_folder_fetch_default_rule(folder);
    when_null(rule, exit);

    retval = 0;

    ipv4 = !(qos_classification_dm_get_ip_version(classification) == 6);

    retval |= fw_rule_set_ipv4(rule, ipv4);
    retval |= fw_rule_set_table(rule, TABLE_MANGLE);
    retval |= fw_rule_set_ip_min_length(rule, qos_classification_dm_get_ip_length_min(classification));
    retval |= fw_rule_set_ip_max_length(rule, qos_classification_dm_get_ip_length_max(classification));

    retval |= qos_classification_activate_set_chain(classification, rule);
    retval |= qos_classification_activate_set_protocol(classification, rule);
    retval |= qos_classification_activate_set_destination_port(classification, rule);
    retval |= qos_classification_activate_set_source_port(classification, rule);
    retval |= qos_classification_activate_set_source_mac(classification, rule);
    retval |= qos_classification_activate_set_destination_mac(classification, rule);
    retval |= qos_classification_activate_set_dscp_check(classification, rule);

    retval |= fw_folder_push_rule(folder, rule);

exit:
    return retval;
}

static bool qos_classification_check_device_based_src(const qos_classification_t* const classification) {
    bool retval = true;
    const char* id = NULL;

    if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
        id = qos_classification_dm_get_source_vendor_class_id(classification);
        when_str_not_empty(id, exit);
    } else {
        id = qos_classification_dm_get_source_vendor_class_id_v6(classification);
        when_str_not_empty(id, exit);
    }

    id = qos_classification_dm_get_source_user_class_id(classification);
    when_str_not_empty(id, exit);

    retval = false;

exit:
    return retval;
}

static bool qos_classification_check_device_based_dst(const qos_classification_t* const classification) {
    bool retval = true;
    const char* id = NULL;

    if(qos_classification_dhcp_type_is_dhcpv4(classification)) {
        id = qos_classification_dm_get_dest_vendor_class_id(classification);
        when_str_not_empty(id, exit);
    } else {
        id = qos_classification_dm_get_dest_vendor_class_id_v6(classification);
        when_str_not_empty(id, exit);
    }

    id = qos_classification_dm_get_dest_user_class_id(classification);
    when_str_not_empty(id, exit);

    retval = false;

exit:
    return retval;
}

#if defined(DEVICE_BASED_QOS)
static int qos_classification_set_ip_based(const qos_classification_t* const classification, bool ip_based) {
    int retval = -1;

    when_null(classification, exit);

    ((qos_classification_t* const) classification)->ip_based = ip_based;

    retval = 0;

exit:
    return retval;
}
#endif

static int qos_classification_activate_set_ip_rule(const qos_classification_t* const classification) {
    int retval = -1;
    bool device_based_classification_dst = false;
    bool device_based_classification_src = false;

    when_null(classification, exit);

    device_based_classification_dst = qos_classification_check_device_based_dst(classification);
    device_based_classification_src = qos_classification_check_device_based_src(classification);

    retval = 0;
    if(!device_based_classification_dst) {
        retval |= qos_classification_activate_set_destination(classification);
    }
    if(!device_based_classification_src) {
        retval |= qos_classification_activate_set_source(classification);
    }
#if defined(DEVICE_BASED_QOS)
    if(device_based_classification_dst || device_based_classification_src) {
        retval |= qos_classification_set_ip_based(classification, false);
        retval |= qos_classification_device_based_activate(classification);
    }
#endif

exit:
    return retval;
}

static int qos_classification_set_query_input_interface(const qos_classification_t* const classification, netmodel_query_t* query) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    ((qos_classification_t* const) classification)->class_intf->query_input_interface = query;
    retval = 0;

exit:
    return retval;
}


static const char* qos_classification_input_interface_get_netdev(const qos_classification_t* const classification) {
    const char* netdev = NULL;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    netdev = ((qos_classification_t* const) classification)->class_intf->netdev_input_interface;

exit:
    return netdev;
}

static int qos_classification_input_interface_set_netdev(const qos_classification_t* const classification, const char* netdev) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    free(((qos_classification_t* const) classification)->class_intf->netdev_input_interface);
    ((qos_classification_t* const) classification)->class_intf->netdev_input_interface = netdev ? strdup(netdev) : NULL;
    retval = 0;

exit:
    return retval;
}

static fw_rule_t* qos_classification_input_interface_get_rule(const qos_classification_t* const classification) {
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    rule = classification->class_intf->rule_input_interface;

exit:
    return rule;
}

static int qos_classification_input_interface_set_rule(const qos_classification_t* const classification, fw_rule_t* rule) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    classification->class_intf->rule_input_interface = rule;
    retval = 0;

exit:
    return retval;
}

void qos_classification_input_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* data, void* classification_userdata) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;
    qos_classification_t* classification = (qos_classification_t*) classification_userdata;
    const char* intf = amxc_var_constcast(cstring_t, data);

    when_null(classification, exit);

    retval = qos_classification_input_interface_set_netdev(classification, intf);
    when_failed(retval, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    // Recreate classification if it was previously created for a different interface name
    // since we cannot delete specific rules from the folder.
    // Interface names barely ever change so reactivating the folder should have a minimal impact.
    rule = qos_classification_input_interface_get_rule(classification);
    if(rule != NULL) {
        qos_classification_reset_folders(classification);
        qos_classification_activate(classification);
        goto exit;
    }

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    if(rule == NULL) {
        SAH_TRACEZ_INFO(ME, "Folder has not been initialized yet for input interface feature");
        return;
    }

    retval |= fw_rule_set_in_interface(rule, intf);
    retval |= fw_folder_push_rule(folder, rule);
    retval |= fw_commit(fw_rule_callback);
    retval |= fw_apply();
    if(retval != 0) {
        qos_classification_activate_failed(classification);
        rule = NULL;
    }

    retval = qos_classification_input_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return;
}

static int qos_classification_activate_feature_input_interface(const qos_classification_t* const classification,
                                                               fw_folder_t* folder) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* interface = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    interface = qos_classification_dm_get_interface(classification);
    when_str_empty(interface, exit);

    // Set feature if Interface parameter is not empty
    retval |= fw_folder_set_feature(folder, FW_FEATURE_IN_INTERFACE);

    interface = qos_classification_input_interface_get_netdev(classification);
    when_str_empty(interface, exit);

    // Set rule in interface if netdev interface is not empty
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_IN_INTERFACE);
    retval |= fw_rule_set_in_interface(rule, interface);
    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

    retval = qos_classification_input_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return retval;
}

static int qos_classification_set_query_output_interface(const qos_classification_t* const classification, netmodel_query_t* query) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    ((qos_classification_t* const) classification)->class_intf->query_output_interface = query;
    retval = 0;

exit:
    return retval;
}

static const char* qos_classification_output_interface_get_netdev(const qos_classification_t* const classification) {
    const char* netdev = NULL;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    netdev = ((qos_classification_t* const) classification)->class_intf->netdev_output_interface;

exit:
    return netdev;
}

static int qos_classification_output_interface_set_netdev(const qos_classification_t* const classification, const char* netdev) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    free(((qos_classification_t* const) classification)->class_intf->netdev_output_interface);
    ((qos_classification_t* const) classification)->class_intf->netdev_output_interface = netdev ? strdup(netdev) : NULL;
    retval = 0;

exit:
    return retval;
}

static fw_rule_t* qos_classification_output_interface_get_rule(const qos_classification_t* const classification) {
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    rule = classification->class_intf->rule_output_interface;

exit:
    return rule;
}

static int qos_classification_output_interface_set_rule(const qos_classification_t* const classification, fw_rule_t* rule) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_intf, exit);

    classification->class_intf->rule_output_interface = rule;
    retval = 0;

exit:
    return retval;
}

void qos_classification_output_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* data, void* classification_userdata) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;
    qos_classification_t* classification = (qos_classification_t*) classification_userdata;
    const char* intf = amxc_var_constcast(cstring_t, data);

    when_null(classification, exit);

    retval = qos_classification_output_interface_set_netdev(classification, intf);
    when_failed(retval, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    // Recreate classification if it was previously created for a different interface name
    // since we cannot delete specific rules from the folder.
    // Interface names barely ever change so reactivating the folder should have a minimal impact.
    rule = qos_classification_output_interface_get_rule(classification);
    if(rule != NULL) {
        qos_classification_reset_folders(classification);
        qos_classification_activate(classification);
        goto exit;
    }

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_OUT_INTERFACE);
    if(rule == NULL) {
        SAH_TRACEZ_INFO(ME, "Folder has not been initialized yet for output interface feature");
        return;
    }

    retval |= fw_rule_set_out_interface(rule, intf);
    retval |= fw_folder_push_rule(folder, rule);
    retval |= fw_commit(fw_rule_callback);
    retval |= fw_apply();
    if(retval != 0) {
        qos_classification_activate_failed(classification);
        rule = NULL;
    }

    retval = qos_classification_output_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return;
}

static int qos_classification_activate_feature_output_interface(const qos_classification_t* const classification,
                                                                fw_folder_t* folder) {
    int retval = -1;
    const char* interface = NULL;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    interface = qos_classification_dm_get_output_interface(classification);
    when_str_empty(interface, exit);

    // Set feature if OutputInterface parameter is not empty
    retval |= fw_folder_set_feature(folder, FW_FEATURE_OUT_INTERFACE);

    interface = qos_classification_output_interface_get_netdev(classification);
    when_str_empty(interface, exit);

    // Set rule out interface if netdev interface is not empty
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_OUT_INTERFACE);
    retval |= fw_rule_set_out_interface(rule, interface);
    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

    qos_classification_output_interface_set_rule(classification, rule);

exit:
    return retval;
}

static int qos_classification_set_query_bridge_input_interface(const qos_classification_t* const classification, netmodel_query_t* query) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    ((qos_classification_t* const) classification)->class_bridge_intf->query_input_interface = query;
    retval = 0;

exit:
    return retval;
}

static const char* qos_classification_bridge_input_interface_get_netdev(const qos_classification_t* const classification) {
    const char* netdev = NULL;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    netdev = ((qos_classification_t* const) classification)->class_bridge_intf->netdev_input_interface;

exit:
    return netdev;
}

static int qos_classification_bridge_input_interface_set_netdev(const qos_classification_t* const classification, const char* netdev) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    free(((qos_classification_t* const) classification)->class_bridge_intf->netdev_input_interface);
    ((qos_classification_t* const) classification)->class_bridge_intf->netdev_input_interface = netdev ? strdup(netdev) : NULL;
    retval = 0;

exit:
    return retval;
}

static fw_rule_t* qos_classification_bridge_input_interface_get_rule(const qos_classification_t* const classification) {
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    rule = classification->class_bridge_intf->rule_input_interface;

exit:
    return rule;
}

static int qos_classification_bridge_input_interface_set_rule(const qos_classification_t* const classification, fw_rule_t* rule) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    classification->class_bridge_intf->rule_input_interface = rule;
    retval = 0;

exit:
    return retval;
}

void qos_classification_bridge_input_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* interface, void* classification_userdata) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;
    qos_classification_t* classification = (qos_classification_t*) classification_userdata;
    const char* intf = amxc_var_constcast(cstring_t, interface);

    when_null(classification, exit);

    retval = qos_classification_bridge_input_interface_set_netdev(classification, intf);
    when_failed(retval, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    // Recreate classification if it was previously created for a different interface name
    // since we cannot delete specific rules from the folder.
    // Interface names barely ever change so reactivating the folder should have a minimal impact.
    rule = qos_classification_bridge_input_interface_get_rule(classification);
    if(rule != NULL) {
        qos_classification_reset_folders(classification);
        qos_classification_activate(classification);
        goto exit;
    }

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_IN);
    if(rule == NULL) {
        SAH_TRACEZ_INFO(ME, "Folder has not been initialized yet for bridge input interface feature");
        goto exit;
    }

    retval |= fw_rule_set_physdev_in(rule, intf);
    retval |= fw_folder_push_rule(folder, rule);
    retval |= fw_commit(fw_rule_callback);
    retval |= fw_apply();
    if(retval != 0) {
        qos_classification_activate_failed(classification);
        rule = NULL;
    }

    retval = qos_classification_bridge_input_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return;
}

static int qos_classification_activate_feature_bridge_input_interface(const qos_classification_t* const classification,
                                                                      fw_folder_t* folder) {
    int retval = -1;
    fw_rule_t* rule = NULL;
    const char* interface = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    interface = qos_classification_dm_get_bridge_input_interface(classification);
    when_str_empty(interface, exit);

    // Set feature if BridgeInputInterface parameter is not empty
    retval |= fw_folder_set_feature(folder, FW_FEATURE_PHYSDEV_IN);

    interface = qos_classification_bridge_input_interface_get_netdev(classification);
    when_str_empty(interface, exit);

    // Set rule in interface if netdev interface is not empty
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_IN);
    retval |= fw_rule_set_physdev_in(rule, interface);
    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

    retval = qos_classification_bridge_input_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return retval;
}

static int qos_classification_set_query_bridge_output_interface(const qos_classification_t* const classification, netmodel_query_t* query) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    ((qos_classification_t* const) classification)->class_bridge_intf->query_output_interface = query;
    retval = 0;

exit:
    return retval;
}

static const char* qos_classification_bridge_output_interface_get_netdev(const qos_classification_t* const classification) {
    const char* netdev = NULL;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    netdev = ((qos_classification_t* const) classification)->class_bridge_intf->netdev_output_interface;

exit:
    return netdev;
}

static int qos_classification_bridge_output_interface_set_netdev(const qos_classification_t* const classification, const char* netdev) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    free(((qos_classification_t* const) classification)->class_bridge_intf->netdev_output_interface);
    ((qos_classification_t* const) classification)->class_bridge_intf->netdev_output_interface = netdev ? strdup(netdev) : NULL;
    retval = 0;

exit:
    return retval;
}

static fw_rule_t* qos_classification_bridge_output_interface_get_rule(const qos_classification_t* const classification) {
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    rule = classification->class_bridge_intf->rule_output_interface;

exit:
    return rule;
}

static int qos_classification_bridge_output_interface_set_rule(const qos_classification_t* const classification, fw_rule_t* rule) {
    int retval = -1;

    when_null(classification, exit);
    when_null(classification->class_bridge_intf, exit);

    classification->class_bridge_intf->rule_output_interface = rule;
    retval = 0;

exit:
    return retval;
}

void qos_classification_bridge_output_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* interface, void* classification_userdata) {
    int retval = 0;
    fw_rule_t* rule = NULL;
    fw_folder_t* folder = NULL;
    qos_classification_t* classification = (qos_classification_t*) classification_userdata;
    const char* intf = amxc_var_constcast(cstring_t, interface);

    when_null(classification, exit);

    retval = qos_classification_bridge_output_interface_set_netdev(classification, intf);
    when_failed(retval, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    // Recreate classification if it was previously created for a different interface name
    // since we cannot delete specific rules from the folder.
    // Interface names barely ever change so reactivating the folder should have a minimal impact.
    rule = qos_classification_bridge_output_interface_get_rule(classification);
    if(rule != NULL) {
        qos_classification_reset_folders(classification);
        qos_classification_activate(classification);
        goto exit;
    }

    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_OUT);
    if(rule == NULL) {
        SAH_TRACEZ_INFO(ME, "Folder has not been initialized yet for output interface feature");
        goto exit;
    }

    retval |= fw_rule_set_physdev_out(rule, intf);
    retval |= fw_folder_push_rule(folder, rule);
    retval |= fw_commit(fw_rule_callback);
    retval |= fw_apply();
    if(retval != 0) {
        qos_classification_activate_failed(classification);
        rule = NULL;
    }

    retval = qos_classification_bridge_output_interface_set_rule(classification, rule);
    if(retval != 0) {
        qos_classification_activate_failed(classification);
    }

exit:
    return;
}

static int qos_classification_activate_feature_bridge_output_interface(const qos_classification_t* const classification,
                                                                       fw_folder_t* folder) {
    int retval = -1;
    const char* interface = NULL;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    interface = qos_classification_dm_get_bridge_output_interface(classification);
    when_str_empty(interface, exit);

    // Set feature if BridgeOutputInterface parameter is not empty
    retval |= fw_folder_set_feature(folder, FW_FEATURE_PHYSDEV_OUT);

    interface = qos_classification_bridge_output_interface_get_netdev(classification);
    when_str_empty(interface, exit);

    // Set rule out interface if netdev interface is not empty
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_PHYSDEV_OUT);
    retval |= fw_rule_set_physdev_out(rule, interface);
    retval |= fw_folder_push_rule(folder, rule);
    when_failed(retval, exit);

    qos_classification_bridge_output_interface_set_rule(classification, rule);

exit:
    return retval;
}

static int qos_classification_activate_feature_dscp(const qos_classification_t* const classification,
                                                    fw_folder_t* folder) {

    int retval = -1;
    int32_t dscp = -1;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    dscp = qos_classification_dm_get_dscp_mark(classification);
    when_true(dscp == -1, exit);

    retval |= fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
    retval |= fw_rule_set_target_dscp(rule, (uint32_t) dscp);
    retval |= fw_folder_push_rule(folder, rule);

exit:
    return retval;
}

static int qos_classification_activate_feature_pbit(const qos_classification_t* const classification,
                                                    fw_folder_t* folder) {

    int retval = -1;
    int32_t ethernet_priority_mark = -1;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    ethernet_priority_mark = qos_classification_dm_get_ethernet_priority_mark(classification);
    when_true(ethernet_priority_mark == -1, exit);

    retval |= fw_folder_set_feature(folder, FW_FEATURE_TARGET);
    rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);

    if(-2 == ethernet_priority_mark) {
        retval |= fw_rule_set_target_pbit(rule, 255);
    } else {
        retval |= fw_rule_set_target_pbit(rule, (uint32_t) ethernet_priority_mark);
    }

    retval |= fw_folder_push_rule(folder, rule);

exit:
    return retval;
}

static qos_node_t* qos_classification_get_scheduler_node(qos_node_t* queue_node) {
    qos_node_t* scheduler_node = NULL;
    scheduler_node = qos_node_find_parent_by_type(queue_node, QOS_NODE_TYPE_SCHEDULER);
    if(scheduler_node == NULL) {
        qos_node_t* parent_queue_node = NULL;
        qos_node_t* nav_queue_node = NULL;
        qos_node_t* shaper_node = NULL;
        nav_queue_node = queue_node;
        parent_queue_node = queue_node;
        while(nav_queue_node) {
            nav_queue_node = qos_node_find_parent_by_type(nav_queue_node, QOS_NODE_TYPE_QUEUE);
            if(nav_queue_node) {
                parent_queue_node = nav_queue_node;
            }
        }
        shaper_node = qos_node_find_parent_by_type(parent_queue_node, QOS_NODE_TYPE_SHAPER);
        if(shaper_node) {
            scheduler_node = qos_node_find_parent_by_type(shaper_node, QOS_NODE_TYPE_SCHEDULER);
        } else {
            scheduler_node = qos_node_find_parent_by_type(parent_queue_node, QOS_NODE_TYPE_SCHEDULER);
        }
    }
    return scheduler_node;
}

static uint32_t qos_classification_get_classkey(qos_queue_t* queue) {
    qos_node_t* scheduler_node = NULL;
    uint32_t classkey_minor = 0;
    uint32_t classkey_major = 0;
    uint32_t classkey = 0;
    qos_node_t* queue_node = NULL;
    const char* classkey_minor_str = NULL;
    const char* classkey_major_str = NULL;
    int ret = 0;

    when_null(queue, exit);
    queue_node = queue->node;
    when_null(queue_node, exit);
    scheduler_node = qos_classification_get_scheduler_node(queue_node);
    when_null(scheduler_node, exit);
    when_null(scheduler_node->dm_object, exit);
    when_null(scheduler_node->dm_object->priv, exit);
    classkey_minor_str = qos_queue_dm_get_classkey_minor(queue);
    when_str_empty(classkey_minor_str, exit);
    classkey_major_str = qos_node_scheduler_get_classkey_major(scheduler_node);
    when_str_empty(classkey_major_str, exit);
    ret = sscanf(classkey_minor_str, "%x", &classkey_minor);
    when_false((ret > 0), exit);
    sscanf(classkey_major_str, "%x", &classkey_major);
    when_false((ret > 0), exit);
    classkey = (((classkey_major & 0x0000FFFF) << 24) | ((classkey_minor & 0x0000FFFF) << 16)) & 0xFFFF0000;
    SAH_TRACEZ_INFO(ME, "minor: %s major: %s classkey: %x", classkey_minor_str, classkey_major_str, classkey);
exit:
    return classkey;
}

static int qos_classification_activate_feature_mark(const qos_classification_t* const classification,
                                                    fw_folder_t* folder) {

    int retval = -1;
    uint32_t queue_key = 0;
    uint32_t class_key = 0;
    uint32_t mask = qos_dm_get_mark_mask();
    const char* traffic_classes = NULL;
    int32_t traffic_class = -1;
    char* tc = NULL;
    char* next_tc = NULL;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    traffic_class = qos_classification_dm_get_traffic_class(classification);
    if(traffic_class > 0) {
        amxd_object_t* queue_object = qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_PATH);
        amxd_object_for_each(instance, it, queue_object) {
            amxd_object_t* queue_instance = amxc_container_of(it, amxd_object_t, it);
            qos_queue_t* queue = (qos_queue_t*) queue_instance->priv;
            if(!queue) {
                continue;
            }
            traffic_classes = qos_queue_dm_get_traffic_classes(queue);
            queue_key = qos_queue_dm_get_queue_key(queue);
            class_key = qos_classification_get_classkey(queue);
            if(traffic_classes && *traffic_classes) {
                tc = (char*) traffic_classes;
                while(tc) {
                    if(traffic_class == (int32_t) strtol(tc, &next_tc, 10)) {
                        retval |= fw_folder_set_feature(folder, FW_FEATURE_TARGET);
                        rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
                        retval |= fw_rule_set_target_mark(rule, queue_key, mask);
                        if(class_key > 0) {
                            retval |= fw_rule_set_target_classify(rule, class_key);
                        }
                        retval |= fw_folder_push_rule(folder, rule);
                        break;
                    }

                    if(next_tc) {
                        if(*next_tc == ',') {
                            next_tc++;
                        } else {
                            next_tc = NULL;
                        }
                    }
                    tc = next_tc;
                }
            }
        }
    }

exit:
    return retval;
}

static int qos_classification_activate_feature_return(const qos_classification_t* const classification,
                                                      fw_folder_t* folder) {

    int retval = -1;
    fw_rule_t* rule = NULL;

    when_null(classification, exit);
    when_null(folder, exit);

    retval = 0;

    if(qos_classification_dm_get_stoptraverse(classification) == true) {
        retval |= fw_folder_set_feature(folder, FW_FEATURE_TARGET);
        rule = fw_folder_fetch_feature_rule(folder, FW_FEATURE_TARGET);
        retval |= fw_rule_set_target_return(rule);
        retval |= fw_folder_push_rule(folder, rule);
    }

exit:
    return retval;
}

static int qos_classification_activate_set_feature_rules(const qos_classification_t* const classification) {
    int retval = -1;
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    retval = 0;

    retval |= qos_classification_activate_feature_input_interface(classification, folder);
    retval |= qos_classification_activate_feature_output_interface(classification, folder);
    retval |= qos_classification_activate_feature_bridge_input_interface(classification, folder);
    retval |= qos_classification_activate_feature_bridge_output_interface(classification, folder);
    retval |= qos_classification_activate_feature_dscp(classification, folder);
    retval |= qos_classification_activate_feature_pbit(classification, folder);
    retval |= qos_classification_activate_feature_mark(classification, folder);
    retval |= qos_classification_activate_feature_return(classification, folder);

exit:
    return retval;
}

static int qos_classification_activate_set_order(const qos_classification_t* const classification) {
    int retval = -1;
    unsigned int order = 0;
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    order = (unsigned int) qos_classification_dm_get_order(classification);
    when_true_status(!order, exit, retval = 0);

    /**
     * Two folders are created per classification (see qos_classification_init):
     *  1. folder4: IPv4 rules.
     *  2. folder6: IPv6 rules.
     *  Map the Order parameter to a folder's order.
     */
    if(qos_classification_dm_get_ip_version(classification) == 6) {
        order += order;
    } else {
        order += order - 1;
    }

    retval = fw_folder_set_order(folder, order);

exit:
    return retval;
}

static int qos_classification_activate_folders(const qos_classification_t* const classification) {
    int retval = -1;
    fw_folder_t* folder = NULL;

    when_null(classification, out);

    folder = qos_classification_get_folder(classification);
    when_null(folder, out);

    SAH_TRACEZ_INFO(ME, "Activating classfication %s", qos_classification_dm_get_alias(classification));

    retval = qos_classification_activate_set_order(classification);
    when_failed(retval, out);

    retval = qos_classification_activate_set_default_rule(classification);
    when_failed(retval, out);

    retval = qos_classification_activate_set_ip_rule(classification);
    when_failed(retval, out);

    retval = qos_classification_activate_set_feature_rules(classification);
    when_failed(retval, out);

    fw_folder_set_enabled(folder, true);
    retval |= fw_commit(fw_rule_callback);
    retval |= fw_apply();

out:
    return retval;
}

int qos_classification_activate(const qos_classification_t* const classification) {
    int retval = -1;

    when_null(classification, exit);

    if(!qos_classification_dm_get_enable(classification)) {
        qos_classification_dm_set_status(classification, QOS_STATUS_DISABLED);
        return 0;
    }

    retval = qos_classification_activate_folders(classification);

    if(retval) {
        qos_classification_activate_failed(classification);
    } else {
        qos_classification_dm_set_status(classification, QOS_STATUS_ENABLED);
        SAH_TRACEZ_INFO(ME, "Activated classification %s", qos_classification_dm_get_alias(classification));
    }

exit:
    return retval;
}

int qos_classification_reset_folders(const qos_classification_t* const classification) {
    int retval = -1;

    when_null(classification, exit);

    retval = 0;
    fw_folder_set_enabled(classification->folder4, 0);
    fw_folder_set_enabled(classification->folder6, 0);
    fw_folder_delete_rules(classification->folder4);
    fw_folder_delete_rules(classification->folder6);
    fw_commit(fw_rule_callback);
    retval |= fw_apply();

    retval |= qos_classification_input_interface_set_rule(classification, NULL);
    retval |= qos_classification_output_interface_set_rule(classification, NULL);

exit:
    return retval;
}

int qos_classification_deactivate(const qos_classification_t* const classification) {
    int retval = -1;

    when_null(classification, exit);

    SAH_TRACEZ_INFO(ME, "Deactivating classfication %s", qos_classification_dm_get_alias(classification));

#if defined(DEVICE_BASED_QOS)
    qos_classification_device_based_deactivate(classification);
#endif

    if(qos_classification_dm_get_status(classification) != QOS_STATUS_ENABLED) {
        retval = 0;
        goto out;
    }

    retval = qos_classification_reset_folders(classification);

out:
    if(retval) {
        qos_classification_dm_set_status(classification, QOS_STATUS_ERROR);
        SAH_TRACEZ_INFO(ME, "Deactivating classfication %s failed", qos_classification_dm_get_alias(classification));
    } else {
        qos_classification_dm_set_status(classification, QOS_STATUS_DISABLED);
        SAH_TRACEZ_INFO(ME, "Deactivated classfication %s", qos_classification_dm_get_alias(classification));
    }

exit:
    return retval;
}

amxd_object_t* qos_classification_get_dm_object(const qos_classification_t* const classification) {
    amxd_object_t* object = NULL;

    when_null(classification, exit);
    object = classification->dm_object;

exit:
    return object;
}

int qos_classification_new(qos_classification_t** classification, amxd_object_t* classification_instance) {
    int retval = -1;
    when_null(classification, exit);
    when_null(classification_instance, exit);
    when_not_null(*classification, exit);
    when_not_null((qos_classification_t*) classification_instance->priv, exit);

    *classification = (qos_classification_t*) calloc(1, sizeof(qos_classification_t));
    when_null(*classification, exit);

    retval = qos_classification_init(*classification, classification_instance);
    when_failed(retval, failed);

    retval = 0;

exit:
    return retval;

failed:
    qos_classification_delete(classification);
    return retval;
}

int qos_classification_dm_set_enable(const qos_classification_t* const classification, const bool enable) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "Enable";
    amxd_status_t status = amxd_status_ok;

    when_null(classification, exit);
    object = qos_classification_get_dm_object(classification);
    when_null(object, exit);

    status = amxd_object_set_value(bool, object, param, enable);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

qos_status_t qos_classification_dm_get_status(const qos_classification_t* const classification) {
    qos_status_t status = QOS_STATUS_DISABLED;
    char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";

    when_null(classification, exit);
    object = qos_classification_get_dm_object(classification);
    when_null(object, exit);

    str_status = amxd_object_get_value(cstring_t, object, param, NULL);
    when_str_empty(str_status, exit);

    status = qos_status_from_string(str_status);
    free(str_status);

exit:
    return status;
}

int qos_classification_dm_set_status(const qos_classification_t* const classification, const qos_status_t status) {
    int retval = -1;
    const char* str_status = NULL;
    amxd_object_t* object = NULL;
    const char* param = "Status";
    amxd_status_t amxd_status = amxd_status_ok;

    if(QOS_STATUS_LAST <= status) {
        goto exit;
    }

    when_null(classification, exit);
    object = qos_classification_get_dm_object(classification);
    when_null(object, exit);

    str_status = qos_status_to_string(status);
    amxd_status = amxd_object_set_value(cstring_t, object, param, str_status);
    when_failed(amxd_status, exit);

    retval = 0;

exit:
    return retval;
}

int qos_classification_dm_set_order(const qos_classification_t* const classification, const uint32_t order) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "Order";
    amxd_status_t status = amxd_status_ok;

    when_null(classification, exit);
    object = qos_classification_get_dm_object(classification);
    when_null(object, exit);
    status = amxd_object_set_value(uint32_t, object, param, order);

    if(order == 0) {
        goto exit;
    }

    if(amxd_status_ok == status) {
        retval = 0;
    }

exit:
    return retval;
}

int qos_classification_dm_set_dest_ip_exclude(const qos_classification_t* const classification, const bool destipexclude) {
    int retval = -1;
    amxd_object_t* object = NULL;
    const char* param = "DestIPExclude";
    amxd_status_t status = amxd_status_ok;

    when_null(classification, exit);
    object = qos_classification_get_dm_object(classification);
    when_null(object, exit);

    status = amxd_object_set_value(bool, object, param, destipexclude);
    when_failed(status, exit);

    retval = 0;

exit:
    return retval;
}

int qos_classification_activate_failed(const qos_classification_t* const classification) {
    int retval = -1;
    fw_folder_t* folder = NULL;

    when_null(classification, exit);

    folder = qos_classification_get_folder(classification);
    when_null(folder, exit);

    fw_folder_delete_rules(folder);
    fw_folder_set_enabled(folder, 0);
    retval = qos_classification_dm_set_status(classification, QOS_STATUS_ERROR_MISCONFIGURED);
    SAH_TRACEZ_ERROR(ME, "Activating classification %s failed", qos_classification_dm_get_alias(classification));

exit:
    return retval;
}

const char* qos_classification_dm_get_output_interface(const qos_classification_t* const classification) {
    amxc_string_t* param = NULL;
    const char* retval = NULL;

    when_null(classification, exit);

    param = qos_config_get_prefixed_parameter("OutputInterface");
    when_null(param, exit);

    retval = amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, amxc_string_get(param, 0)));

exit:
    amxc_string_delete(&param);
    return retval;
}

const char* qos_classification_dm_get_direction(const qos_classification_t* const classification) {
    amxc_string_t* param = NULL;
    const char* retval = NULL;

    when_null(classification, exit);

    param = qos_config_get_prefixed_parameter("Direction");
    when_null(param, exit);

    retval = amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, amxc_string_get(param, 0)));

exit:
    amxc_string_delete(&param);
    return retval;
}

const char* qos_classification_dm_get_bridge_output_interface(const qos_classification_t* const classification) {
    amxc_string_t* param = NULL;
    const char* bridge_output_interface = NULL;

    when_null(classification, exit);

    param = qos_config_get_prefixed_parameter("BridgeOutputInterface");
    when_null(param, exit);

    bridge_output_interface = amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, amxc_string_get(param, 0)));

exit:
    amxc_string_delete(&param);
    return bridge_output_interface;
}

const char* qos_classification_dm_get_bridge_input_interface(const qos_classification_t* const classification) {
    amxc_string_t* param = NULL;
    const char* bridge_input_interface = NULL;

    when_null(classification, exit);

    param = qos_config_get_prefixed_parameter("BridgeInputInterface");
    when_null(param, exit);

    bridge_input_interface = amxc_var_constcast(cstring_t, amxd_object_get_param_value(classification->dm_object, amxc_string_get(param, 0)));

exit:
    amxc_string_delete(&param);
    return bridge_input_interface;
}
