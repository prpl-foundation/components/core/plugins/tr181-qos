/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <qoscommon/qos-queue.h>
#include <qoscommon/qos-queue-stats.h>
#include <qosnode/qos-node.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <debug/sahtrace.h>

#include "tr181-qos.h"
#include "qos-controller.h"
#include "qos-assert.h"
#include "qos.h"
#include "qos-classification.h"

#define ME "ctrl"

inline amxd_object_t* qos_controller_get_qos_object(void) {
    return amxd_dm_findf(qos_get_dm(), QOS_TR181_DEVICE_QOS_PATH);
}

inline qos_t* qos_controller_get_qos(void) {
    return qos_get_dm_object_priv_data(qos_controller_get_qos_object());
}

inline netmodel_query_t* qos_controller_get_query(netmodel_callback_t func, void* qos_type, const char* intf_name) {
    return netmodel_openQuery_getFirstParameter(intf_name, "tr181-qos", "NetDevName", "netdev-bound && ( eth_intf || xpon )", "down", func, qos_type);
}

static inline void qos_controller_activate_shapers(void) {

    amxd_object_for_each(instance, it, qos_get_object(QOS_TR181_DEVICE_QOS_SHAPER_PATH)) {
        amxd_object_t* shaper_instance = amxc_container_of(it, amxd_object_t, it);
        qos_shaper_t* shaper = (qos_shaper_t*) shaper_instance->priv;

        shaper->query = qos_controller_get_query(qos_shaper_interface_netdevname_changed, shaper, qos_shaper_dm_get_interface(shaper));
    }
}

static inline void qos_controller_activate_schedulers(void) {

    amxd_object_for_each(instance, it, qos_get_object(QOS_TR181_DEVICE_QOS_SCHEDULER_PATH)) {
        amxd_object_t* scheduler_instance = amxc_container_of(it, amxd_object_t, it);
        qos_scheduler_t* scheduler = (qos_scheduler_t*) scheduler_instance->priv;

        scheduler->query = qos_controller_get_query(qos_scheduler_interface_netdevname_changed, scheduler, qos_scheduler_dm_get_interface(scheduler));
    }
}

static inline void qos_controller_activate_queues(void) {

    amxd_object_for_each(instance, it, qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_PATH)) {
        amxd_object_t* queue_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_t* queue = (qos_queue_t*) queue_instance->priv;

        queue->query = qos_controller_get_query(qos_queue_interface_netdevname_changed, queue, qos_queue_dm_get_interface(queue));
    }
}

static inline void qos_controller_activate_queue_stats(void) {

    amxd_object_for_each(instance, it, qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH)) {
        amxd_object_t* queue_stats_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_stats_t* queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;

        queue_stats->query = qos_controller_get_query(qos_queue_stats_interface_netdevname_changed, queue_stats, qos_queue_stats_dm_get_interface(queue_stats));
    }
}

static void qos_controller_activate(void) {
    qos_controller_activate_shapers();
    qos_controller_activate_schedulers();
    qos_controller_activate_queues();
    qos_controller_activate_queue_stats();
}

int qos_controller_init(void) {
    int retval = -1;
    qos_t* qos = NULL;

    retval = qos_firewall_init();
    retval = (retval ? 0 : retval); //TODO: remove when qos_firewall_init checks on IPv6.

    if(retval != 0) {
        raise(SIGTERM); // Make sure the qos application stops.
        // alternativly amxrt_el_stop() can be used - needs extra library dependency
        SAH_TRACEZ_ERROR(ME, "Failed to initialize firewall");
        goto exit;
    }
    qos = qos_controller_get_qos();

    if(qos == NULL) {
        amxd_object_t* qos_object = NULL;

        qos_object = qos_controller_get_qos_object();
        when_null(qos_object, exit);

        retval = qos_new(&qos, qos_object);
        when_failed(retval, exit);
        SAH_TRACEZ_NOTICE(ME, "qos object created");

        qos_controller_activate();
        SAH_TRACEZ_NOTICE(ME, "qos activated");
    }

    retval = 0;

    SAH_TRACEZ_NOTICE(ME, "controller initialized");

exit:
    return retval;
}

int qos_controller_deinit(void) {
    int retval = -1;
    qos_t* qos = NULL;

    qos = qos_controller_get_qos();

    retval = qos_delete(&qos);
    return retval;
}

int qos_controller_dm_queue_added(amxd_object_t* queue_instance) {
    int retval = -1;
    qos_queue_t* queue = NULL;

    when_null(queue_instance, exit);

    retval = qos_queue_new(&queue, queue_instance);
    when_failed(retval, exit);

    queue->query = qos_controller_get_query(qos_queue_interface_netdevname_changed, queue, qos_queue_dm_get_interface(queue));
    retval = (queue->query != NULL) ? 0 : -1;

exit:
    return retval;
}

int qos_controller_dm_queue_removed(amxd_object_t* queue_instance) {
    int retval = -1;
    qos_queue_t* queue = NULL;

    when_null(queue_instance, exit);

    queue = (qos_queue_t*) queue_instance->priv;
    when_null(queue, exit);

    netmodel_closeQuery(queue->query);
    queue->query = NULL;

    retval = qos_queue_delete(&queue);

exit:
    return retval;
}

int qos_controller_dm_queue_changed(amxd_object_t* queue_instance) {
    int retval = -1;
    qos_t* qos = NULL;
    qos_queue_t* queue = NULL;

    qos = qos_controller_get_qos();

    when_null(qos, exit);
    when_null(queue_instance, exit);

    queue = (qos_queue_t*) queue_instance->priv;
    when_null(queue, exit);

    if(qos_queue_dm_get_enable(queue)) {
        qos_queue_deactivate(queue);
        qos_queue_activate(queue);
    } else {
        if(qos_queue_dm_get_status(queue) != QOS_STATUS_DISABLED) {
            qos_queue_deactivate(queue);
        }
    }

    retval = 0;

exit:
    return retval;
}

int qos_controller_dm_shaper_added(amxd_object_t* shaper_instance) {
    int retval = -1;
    qos_shaper_t* shaper = NULL;

    when_null(shaper_instance, exit);

    retval = qos_shaper_new(&shaper, shaper_instance);
    when_failed(retval, exit);

    shaper->query = qos_controller_get_query(qos_shaper_interface_netdevname_changed, shaper, qos_shaper_dm_get_interface(shaper));
    retval = (shaper->query != NULL) ? 0 : -1;

exit:
    return retval;
}

int qos_controller_dm_shaper_removed(amxd_object_t* shaper_instance) {
    int retval = -1;
    qos_t* qos = NULL;
    qos_shaper_t* shaper = NULL;

    qos = qos_controller_get_qos();

    when_null(qos, exit);
    when_null(shaper_instance, exit);

    shaper = (qos_shaper_t*) shaper_instance->priv;
    when_null(shaper, exit);

    netmodel_closeQuery(shaper->query);
    shaper->query = NULL;

    retval = qos_shaper_delete(&shaper);

exit:
    return retval;
}

int qos_controller_dm_shaper_changed(amxd_object_t* shaper_instance) {
    int retval = -1;
    qos_shaper_t* shaper = NULL;
    const char* intf_name;

    when_null(shaper_instance, exit);

    shaper = (qos_shaper_t*) shaper_instance->priv;
    when_null(shaper, exit);

    if(qos_shaper_dm_get_enable(shaper)) {
        qos_shaper_deactivate(shaper);
        intf_name = qos_shaper_get_interface_name(shaper);
        if(intf_name && intf_name[0]) {
            qos_shaper_activate(shaper);
        }
    } else {
        if(qos_shaper_dm_get_status(shaper) != QOS_STATUS_DISABLED) {
            qos_shaper_deactivate(shaper);
        }
    }

    retval = 0;

exit:
    return retval;
}

int qos_controller_dm_scheduler_added(amxd_object_t* scheduler_instance) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;

    when_null(scheduler_instance, exit);

    retval = qos_scheduler_new(&scheduler, scheduler_instance);
    when_failed(retval, exit);

    scheduler->query = qos_controller_get_query(qos_scheduler_interface_netdevname_changed, scheduler, qos_scheduler_dm_get_interface(scheduler));
    retval = (scheduler->query != NULL) ? 0 : -1;

exit:
    return retval;
}

int qos_controller_dm_scheduler_removed(amxd_object_t* scheduler_instance) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;

    when_null(scheduler_instance, exit);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    when_null(scheduler, exit);

    netmodel_closeQuery(scheduler->query);
    scheduler->query = NULL;

    retval = qos_scheduler_delete(&scheduler);

exit:
    return retval;
}

int qos_controller_dm_scheduler_changed(amxd_object_t* scheduler_instance) {
    int retval = -1;
    qos_scheduler_t* scheduler = NULL;
    when_null(scheduler_instance, exit);

    scheduler = (qos_scheduler_t*) scheduler_instance->priv;
    when_null(scheduler, exit);

    if(!qos_scheduler_dm_get_enable(scheduler)) {
        if(qos_scheduler_dm_get_status(scheduler) != QOS_STATUS_DISABLED) {
            qos_scheduler_deactivate(scheduler);
        }
    } else {
        qos_scheduler_deactivate(scheduler);
        qos_scheduler_activate(scheduler);
    }

    retval = 0;

exit:
    return retval;
}

int qos_controller_dm_classification_added(amxd_object_t* classification_instance) {
    int retval = -1;
    qos_classification_t* classification = NULL;

    when_null(classification_instance, exit);

    retval = qos_classification_new(&classification, classification_instance);
    when_failed(retval, exit);

    retval = qos_controller_dm_classification_changed(classification_instance);

exit:
    return retval;
}

//! [architecture controller]
int qos_controller_dm_classification_changed(amxd_object_t* classification_instance) {
    int retval = -1;
    qos_classification_t* classification = NULL;

    when_null(classification_instance, exit);
    classification = (qos_classification_t*) classification_instance->priv;
    when_null(classification, exit);

    if(qos_classification_dm_get_enable(classification)) {
        qos_classification_deactivate(classification);
        qos_classification_activate(classification);
    } else {
        if(qos_classification_dm_get_status(classification) != QOS_STATUS_DISABLED) {
            qos_classification_deactivate(classification);
        }
    }

    retval = 0;

exit:
    return retval;
}
//! [architecture controller]

int qos_controller_dm_classification_removed(amxd_object_t* classification_instance) {
    int retval = -1;
    qos_classification_t* classification = NULL;

    when_null(classification_instance, exit);

    classification = (qos_classification_t*) classification_instance->priv;
    when_null(classification, exit);

    qos_classification_deactivate(classification);
    retval = qos_classification_delete(&classification);

exit:
    return retval;
}

int qos_controller_dm_queue_stats_added(amxd_object_t* queue_stats_instance) {
    int retval = -1;
    qos_queue_stats_t* stats = NULL;

    when_null(queue_stats_instance, exit);

    retval = qos_queue_stats_new(&stats, queue_stats_instance);
    when_failed(retval, exit);

    stats->query = qos_controller_get_query(qos_queue_stats_interface_netdevname_changed, stats, qos_queue_stats_dm_get_interface(stats));
    retval = (stats->query != NULL) ? 0 : -1;

exit:
    return retval;
}

int qos_controller_dm_queue_stats_changed(amxd_object_t* queue_stats_instance) {
    int retval = -1;
    qos_t* qos = NULL;
    qos_queue_stats_t* stats = NULL;

    qos = qos_controller_get_qos();

    when_null(qos, exit);
    when_null(queue_stats_instance, exit);

    stats = (qos_queue_stats_t*) queue_stats_instance->priv;
    when_null(stats, exit);

    if(qos_queue_stats_dm_get_enable(stats)) {
        qos_queue_stats_deactivate(stats);
        qos_queue_stats_activate(stats);
    } else {
        if(qos_queue_stats_dm_get_status(stats) != QOS_STATUS_DISABLED) {
            qos_queue_stats_deactivate(stats);
        }
    }
    retval = 0;

exit:
    return retval;
}

int qos_controller_dm_queue_stats_removed(amxd_object_t* queue_stats_instance) {
    int retval = -1;
    qos_queue_stats_t* queue_stats = NULL;

    when_null(queue_stats_instance, exit);

    queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;
    when_null(queue_stats, exit);

    netmodel_closeQuery(queue_stats->query);
    queue_stats->query = NULL;

    retval = qos_queue_stats_delete(&queue_stats);

exit:
    return retval;
}
