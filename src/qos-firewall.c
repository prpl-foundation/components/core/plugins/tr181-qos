/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <fwrules/fw_rule.h>
#include <fwinterface/interface.h>

#include "qos-assert.h"
#include "qos-firewall.h"
#include "qos-config.h"

#define CHAIN_PREROUTING "PREROUTING"
#define CHAIN_POSTROUTING "POSTROUTING"
#define CHAIN_OUTPUT "OUTPUT"
#define CHAIN_INPUT "INPUT"
#define CHAIN_FORWARD "FORWARD"

static int qos_firewall_add_chain(const char* chain) {
    return fw_add_chain(qos_config_get_iptables_target(chain), TABLE_MANGLE, false);
}

static int qos_firewall_add_chain6(const char* chain) {
    return fw_add_chain(qos_config_get_ip6tables_target(chain), TABLE_MANGLE, true);
}

static int qos_firewall_add_ipv4_chains(void) {
    int retval = 0;

    retval |= qos_firewall_add_chain(QOS_CONFIG_IPTABLES_PREROUTING_TARGET);
    retval |= qos_firewall_add_chain(QOS_CONFIG_IPTABLES_INPUT_TARGET);
    retval |= qos_firewall_add_chain(QOS_CONFIG_IPTABLES_POSTROUTING_TARGET);
    retval |= qos_firewall_add_chain(QOS_CONFIG_IPTABLES_OUTPUT_TARGET);
    retval |= qos_firewall_add_chain(QOS_CONFIG_IPTABLES_FORWARD_TARGET);

    return retval;
}

static int qos_firewall_add_ipv6_chains(void) {
    int retval = 0;

    retval |= qos_firewall_add_chain6(QOS_CONFIG_IP6TABLES_PREROUTING_TARGET);
    retval |= qos_firewall_add_chain6(QOS_CONFIG_IP6TABLES_INPUT_TARGET);
    retval |= qos_firewall_add_chain6(QOS_CONFIG_IP6TABLES_POSTROUTING_TARGET);
    retval |= qos_firewall_add_chain6(QOS_CONFIG_IP6TABLES_OUTPUT_TARGET);
    retval |= qos_firewall_add_chain6(QOS_CONFIG_IP6TABLES_FORWARD_TARGET);

    return retval;
}

static int qos_firewall_add_ipv4_chain_target_rules(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    when_failed(retval, exit);
    when_null(rule, exit);

    retval |= fw_rule_set_table(rule, TABLE_MANGLE);
    retval |= fw_rule_set_ipv4(rule, true);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_PREROUTING);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_iptables_target(QOS_CONFIG_IPTABLES_PREROUTING_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_INPUT);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_iptables_target(QOS_CONFIG_IPTABLES_INPUT_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_POSTROUTING);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_iptables_target(QOS_CONFIG_IPTABLES_POSTROUTING_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_OUTPUT);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_iptables_target(QOS_CONFIG_IPTABLES_OUTPUT_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_FORWARD);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_iptables_target(QOS_CONFIG_IPTABLES_FORWARD_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

error:
    fw_rule_delete(&rule);
exit:
    return retval;
}

static int qos_firewall_add_ipv6_chain_target_rules(void) {
    int retval = 0;
    fw_rule_t* rule = NULL;

    retval = fw_rule_new(&rule);
    when_failed(retval, exit);
    when_null(rule, exit);

    retval |= fw_rule_set_table(rule, TABLE_MANGLE);
    retval |= fw_rule_set_ipv6(rule, true);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_PREROUTING);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_ip6tables_target(QOS_CONFIG_IP6TABLES_PREROUTING_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_INPUT);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_ip6tables_target(QOS_CONFIG_IP6TABLES_INPUT_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_POSTROUTING);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_ip6tables_target(QOS_CONFIG_IP6TABLES_POSTROUTING_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_OUTPUT);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_ip6tables_target(QOS_CONFIG_IP6TABLES_OUTPUT_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

    retval |= fw_rule_set_chain(rule, CHAIN_FORWARD);
    retval |= fw_rule_set_target_chain(rule, qos_config_get_ip6tables_target(QOS_CONFIG_IP6TABLES_FORWARD_TARGET));
    retval |= fw_append_rule(rule);
    when_failed(retval, error);

error:
    fw_rule_delete(&rule);
exit:
    return retval;
}

static int qos_firewall_init_ipv4(void) {
    int retval = -1;

    retval = qos_firewall_add_ipv4_chains();
    when_failed(retval, exit);

    retval = qos_firewall_add_ipv4_chain_target_rules();

exit:
    return retval;
}

static int qos_firewall_init_ipv6(void) {
    int retval = -1;

    retval = qos_firewall_add_ipv6_chains();
    when_failed(retval, exit);

    retval = qos_firewall_add_ipv6_chain_target_rules();

exit:
    return retval;
}

int qos_firewall_init(void) {
    int retval = -1;
    amxc_var_t* config = qos_get_config();

    retval = qos_firewall_init_ipv4();
    when_failed(retval, exit);

    if(!GET_BOOL(config, "ip6tables_disable")) {
        retval = qos_firewall_init_ipv6();
        when_failed(retval, exit);
    }

    fw_apply();

exit:
    return retval;
}

