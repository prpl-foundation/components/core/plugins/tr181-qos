/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "qos.h"
#include "qos-assert.h"
#include <qosnode/qos-node.h>
#include "qos-module-mngr.h"
#include "qos-controller.h"
#include <qosmodule/api.h>

int qos_queue_stats_activate(const qos_queue_stats_t* const queue_stats) {
    int retval = -1;
    qos_queue_t* queue;
    amxd_object_t* queue_object;
    const char* queue_path = NULL;

    when_null(queue_stats, exit);
    queue_path = qos_queue_stats_dm_get_queue(queue_stats);

    when_null(queue_path, failed);
    queue_object = qos_get_object(queue_path);
    when_null(queue_object, failed);
    queue = queue_object->priv;
    when_null(queue, failed);

    retval = qos_queue_stats_dm_set_status(queue_stats, QOS_STATUS_ENABLED);

exit:
    return retval;

failed:
    retval = qos_queue_stats_dm_set_status(queue_stats, QOS_STATUS_ERROR);
    return retval;
}

int qos_queue_stats_deactivate(const qos_queue_stats_t* const queue_stats) {
    int retval = -1;
    retval = qos_queue_stats_dm_set_status(queue_stats, QOS_STATUS_DISABLED);
    return retval;
}

void qos_queue_stats_update_stats(amxd_object_t* queue_stats, amxc_var_t* stats) {
    amxd_param_t* OutputPackets = amxd_object_get_param_def(queue_stats, "OutputPackets");
    amxd_param_t* OutputBytes = amxd_object_get_param_def(queue_stats, "OutputBytes");
    amxd_param_t* DroppedPackets = amxd_object_get_param_def(queue_stats, "DroppedPackets");
    amxd_param_t* DroppedBytes = amxd_object_get_param_def(queue_stats, "DroppedBytes");

    amxc_var_set(uint32_t, &OutputPackets->value, GETP_UINT32(stats, "TxPackets"));
    amxc_var_set(uint32_t, &OutputBytes->value, GETP_UINT32(stats, "TxBytes"));
    amxc_var_set(uint32_t, &DroppedPackets->value, GETP_UINT32(stats, "DroppedPackets"));
    amxc_var_set(uint32_t, &DroppedBytes->value, GETP_UINT32(stats, "DroppedBytes"));
}

amxd_object_t* qos_queue_stats_asked(amxd_object_t* object) {
    const bool enable = GET_BOOL(amxd_object_get_param_value(object, "Enable"), NULL);
    amxd_param_t* status = amxd_object_get_param_def(object, "Status");
    const char* queue_path = NULL;
    qos_queue_t* queue;
    amxd_object_t* queue_object;
    amxc_var_t data;
    amxc_var_t ret;
    const char* queue_ctrl = NULL;
    int32_t retval = -1;
    char* path = NULL;

    when_failed(amxc_var_init(&ret), failed);
    when_failed(amxc_var_init(&data), failed);
    when_null(object, exit);

    if(!enable) {
        goto exit;
    }

    queue_path = GET_CHAR(amxd_object_get_param_value(object, "Queue"), NULL);
    when_str_empty(queue_path, failed);
    queue_object = qos_get_object(queue_path);
    when_null(queue_object, failed);
    queue = queue_object->priv;

    when_null(queue, failed);
    when_null(queue->node, failed);

    queue_ctrl = qos_queue_dm_get_queue_ctrl(queue);

    path = amxd_object_get_path(queue->node->dm_object, 0);
    amxc_var_set_cstring_t(&data, path);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_RETRIEVE_STATS, queue_ctrl, &data, &ret);

    when_failed(retval, failed);
    amxc_var_set(cstring_t, &status->value, "Enabled");
    qos_queue_stats_update_stats(object, &ret);

exit:
    free(path);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;

failed:
    amxc_var_set(cstring_t, &status->value, "Error");
    free(path);
    amxc_var_clean(&data);
    amxc_var_clean(&ret);
    return object;
}

void qos_queue_stats_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    qos_queue_stats_t* queue_stats = (qos_queue_stats_t*) priv;
    const char* intf_name;

    when_null(queue_stats, exit);
    when_null(data, exit);

    intf_name = GET_CHAR(data, NULL);
    if(qos_queue_stats_dm_get_enable(queue_stats)) {
        qos_queue_stats_deactivate(queue_stats);
    }
    qos_queue_stats_set_interface_name(queue_stats, intf_name);
    qos_controller_dm_queue_stats_changed(qos_queue_stats_get_dm_object(queue_stats));
exit:
    return;
}
