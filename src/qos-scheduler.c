/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>

#include <qoscommon/qos-scheduler.h>
#include <qos-assert.h>
#include <qosnode/qos-node.h>
#include <qosmodule/api.h>

#include "tr181-qos.h"
#include "qos.h"
#include "qos-module-mngr.h"
#include "qos-controller.h"

static inline const char* qos_get_scheduler_ctrl(const qos_scheduler_t* const scheduler) {
    if(!scheduler) {
        return NULL;
    }
    return amxc_var_constcast(cstring_t, amxd_object_get_param_value(scheduler->dm_object, CONTROLLER));
}

int qos_scheduler_activate(const qos_scheduler_t* const scheduler) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;
    const char* scheduler_ctrl = NULL;
    char* path;

    when_null(scheduler, exit);
    when_null(scheduler->node, exit);
    when_failed(amxc_var_init(&ret), exit);
    when_failed(amxc_var_init(&data), exit);

    scheduler_ctrl = qos_get_scheduler_ctrl(scheduler);

    path = amxd_object_get_path(scheduler->node->dm_object, 0);
    amxc_var_set_cstring_t(&data, path);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_ACTIVATE_SCHEDULER, scheduler_ctrl, &data, &ret);

    if(retval == 0) {
        qos_scheduler_dm_set_status(scheduler, QOS_STATUS_ENABLED);
        qos_activate_children(scheduler->node);
    } else {
        if(retval == -1) {
            qos_scheduler_dm_set_status(scheduler, QOS_STATUS_ERROR);
        } else {
            qos_scheduler_dm_set_status(scheduler, QOS_STATUS_ERROR_MISCONFIGURED);
            retval = -1;
        }
    }

    free(path);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

exit:
    return retval;
}

int qos_scheduler_deactivate(const qos_scheduler_t* const scheduler) {
    int retval = -1;
    amxc_var_t data;
    amxc_var_t ret;
    const char* scheduler_ctrl = NULL;
    char* path;

    when_null(scheduler, exit);
    when_null(scheduler->node, exit);
    when_failed(amxc_var_init(&ret), exit);
    when_failed(amxc_var_init(&data), exit);

    scheduler_ctrl = qos_get_scheduler_ctrl(scheduler);

    path = amxd_object_get_path(scheduler->node->dm_object, 0);
    amxc_var_set_cstring_t(&data, path);

    qos_deactivate_children(scheduler->node);

    retval = qos_module_exec_api_function(QOS_MODULE_API_FUNC_DEACTIVATE_SCHEDULER, scheduler_ctrl, &data, &ret);

    if(retval == 0) {
        qos_scheduler_dm_set_status(scheduler, QOS_STATUS_DISABLED);
    } else {
        if(retval == -1) {
            qos_scheduler_dm_set_status(scheduler, QOS_STATUS_ERROR);
        } else {
            qos_scheduler_dm_set_status(scheduler, QOS_STATUS_ERROR_MISCONFIGURED);
            retval = -1;
        }
    }

    free(path);
    amxc_var_clean(&ret);
    amxc_var_clean(&data);

exit:
    return retval;
}

void qos_scheduler_interface_netdevname_changed(UNUSED const char* sig_name, const amxc_var_t* data, void* priv) {
    qos_scheduler_t* scheduler = (qos_scheduler_t*) priv;
    const char* intf_name;

    when_null(scheduler, exit);
    when_null(data, exit);

    intf_name = GET_CHAR(data, NULL);
    if(qos_scheduler_dm_get_enable(scheduler)) {
        qos_scheduler_deactivate(scheduler);
    }
    qos_scheduler_set_interface_name(scheduler, intf_name);
    qos_controller_dm_scheduler_changed(qos_scheduler_get_dm_object(scheduler));
exit:
    return;
}
