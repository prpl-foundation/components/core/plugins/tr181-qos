/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2021 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_action.h>

#include <qosnode/qos-node.h>
#include <qoscommon/qos-queue.h>
#include <qoscommon/qos-queue-stats.h>
#include <qoscommon/qos-shaper.h>

#include <netmodel/common_api.h>

#include "tr181-qos.h"
#include "qos.h"
#include "qos-assert.h"
#include "qos-module-mngr.h"

#include "dm-qos-node.h"

static int qos_create_queue_stats(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* queue_stats_object = NULL;

    when_null(qos, exit);

    queue_stats_object = qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH);
    when_null_status(queue_stats_object, exit, retval = 0);

    amxd_object_for_each(instance, it, queue_stats_object) {
        amxd_object_t* queue_stats_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_stats_t* queue_stats = NULL;

        if(!queue_stats_instance) {
            continue;
        }

        queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;

        if(queue_stats) {
            continue;
        }

        retval = qos_queue_stats_new(&queue_stats, queue_stats_instance);
        when_failed(retval, exit);
        retval = qos_dm_create_node(queue_stats_instance);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_cleanup_queue_stats(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* queue_stats_object = NULL;

    when_null(qos, exit);

    queue_stats_object = qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_STATS_PATH);
    when_null_status(queue_stats_object, exit, retval = 0);

    amxd_object_for_each(instance, it, queue_stats_object) {
        amxd_object_t* queue_stats_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_stats_t* queue_stats = (qos_queue_stats_t*) queue_stats_instance->priv;

        if(!queue_stats) {
            continue;
        }

        netmodel_closeQuery(queue_stats->query);
        queue_stats->query = NULL;

        retval = qos_queue_stats_delete(&queue_stats);
        when_failed(retval, exit);

    }

    retval = 0;

exit:
    return retval;
}

static int qos_create_shapers(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* shaper_object = NULL;

    when_null(qos, exit);

    shaper_object = qos_get_object(QOS_TR181_DEVICE_QOS_SHAPER_PATH);
    when_null_status(shaper_object, exit, retval = 0);

    amxd_object_for_each(instance, it, shaper_object) {
        amxd_object_t* shaper_instance = amxc_container_of(it, amxd_object_t, it);
        qos_shaper_t* shaper = (qos_shaper_t*) shaper_instance->priv;

        if(!shaper_instance || shaper) {
            continue;
        }

        retval = qos_shaper_new(&shaper, shaper_instance);
        when_failed(retval, exit);
        retval = qos_dm_create_node(shaper_instance);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_cleanup_shapers(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* shaper_object = NULL;

    when_null(qos, exit);

    shaper_object = qos_get_object(QOS_TR181_DEVICE_QOS_SHAPER_PATH);
    when_null_status(shaper_object, exit, retval = 0);

    amxd_object_for_each(instance, it, shaper_object) {
        amxd_object_t* shaper_instance = amxc_container_of(it, amxd_object_t, it);
        qos_shaper_t* shaper = (qos_shaper_t*) shaper_instance->priv;

        if(!shaper) {
            continue;
        }

        netmodel_closeQuery(shaper->query);
        shaper->query = NULL;

        retval = qos_shaper_delete(&shaper);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

//! [qos queues]
static int qos_create_queues(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* queue_object = NULL;

    when_null(qos, exit);

    queue_object = qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_PATH);
    when_null_status(queue_object, exit, retval = 0);

    amxd_object_for_each(instance, it, queue_object) {
        amxd_object_t* queue_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_t* queue = NULL;

        if(!queue_instance) {
            continue;
        }

        queue = (qos_queue_t*) queue_instance->priv;

        if(queue) {
            continue;
        }

        retval = qos_queue_new(&queue, queue_instance);
        when_failed(retval, exit);
        retval = qos_dm_create_node(queue_instance);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}
//! [qos queues]

static int qos_cleanup_queues(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* queue_object = NULL;

    when_null(qos, exit);

    queue_object = qos_get_object(QOS_TR181_DEVICE_QOS_QUEUE_PATH);
    when_null_status(queue_object, exit, retval = 0);

    amxd_object_for_each(instance, it, queue_object) {
        amxd_object_t* queue_instance = amxc_container_of(it, amxd_object_t, it);
        qos_queue_t* queue = (qos_queue_t*) queue_instance->priv;

        if(!queue) {
            continue;
        }

        netmodel_closeQuery(queue->query);
        queue->query = NULL;

        retval = qos_queue_delete(&queue);
        when_failed(retval, exit);

    }

    retval = 0;

exit:
    return retval;
}

//! [qos_create_nodes]
static int qos_create_nodes(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* node_object = NULL;

    when_null(qos, exit);

    node_object = qos_get_object(QOS_TR181_DEVICE_QOS_NODE_PATH);
    when_null_status(node_object, exit, retval = 0);

    amxd_object_for_each(instance, it, node_object) {
        amxd_object_t* node_instance = amxc_container_of(it, amxd_object_t, it);
        qos_node_t* node = NULL;

        if(!node_instance) {
            continue;
        }

        node = (qos_node_t*) node_instance->priv;

        if(node) {
            continue;
        }

        retval = qos_node_new(&node, node_instance);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}
//! [qos_create_nodes]

static int qos_cleanup_nodes(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* node_object = NULL;

    when_null(qos, exit);

    node_object = qos_get_object(QOS_TR181_DEVICE_QOS_NODE_PATH);
    when_null_status(node_object, exit, retval = 0);

    amxd_object_for_each(instance, it, node_object) {
        amxd_object_t* node_instance = amxc_container_of(it, amxd_object_t, it);
        qos_node_t* node = NULL;

        if(!node_instance) {
            continue;
        }

        node = (qos_node_t*) node_instance->priv;

        if(!node) {
            continue;
        }

        retval = qos_node_delete(&node);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static amxd_object_t* qos_get_classification_object(const qos_t* const qos) {
    amxd_object_t* qos_object = NULL;
    amxd_object_t* classification_object = NULL;

    when_null(qos, exit);
    qos_object = qos_get_dm_object(qos);
    when_null(qos_object, exit);

    classification_object = amxd_object_get_child(qos_object, QOS_TR181_DEVICE_QOS_CLASSIFICATION_OBJECT_NAME);

exit:
    return classification_object;
}

static int qos_create_classifications(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* classification_object = NULL;

    when_null(qos, exit);

    classification_object = qos_get_classification_object(qos);
    when_null_status(classification_object, exit, retval = 0);

    amxd_object_for_each(instance, it, classification_object) {
        amxd_object_t* classification_instance = amxc_container_of(it, amxd_object_t, it);

        if(!classification_instance) {
            continue;
        }

        qos_classification_t* classification = (qos_classification_t*) classification_instance->priv;

        if(classification) {
            continue;
        }

        retval = qos_classification_new(&classification, classification_instance);
        when_failed(retval, exit);

        qos_classification_activate(classification);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_cleanup_classifications(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* classification_object = NULL;

    when_null(qos, exit);

    classification_object = qos_get_classification_object(qos);
    when_null_status(classification_object, exit, retval = 0);

    amxd_object_for_each(instance, it, classification_object) {
        amxd_object_t* classification_instance = amxc_container_of(it, amxd_object_t, it);
        qos_classification_t* classification = (qos_classification_t*) classification_instance->priv;

        if(!classification) {
            continue;
        }

        retval = qos_classification_delete(&classification);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_create_schedulers(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* scheduler_object = NULL;

    when_null(qos, exit);

    scheduler_object = qos_get_object(QOS_TR181_DEVICE_QOS_SCHEDULER_PATH);
    when_null_status(scheduler_object, exit, retval = 0);

    amxd_object_for_each(instance, it, scheduler_object) {
        amxd_object_t* scheduler_instance = amxc_container_of(it, amxd_object_t, it);

        if(!scheduler_instance) {
            continue;
        }

        qos_scheduler_t* scheduler = (qos_scheduler_t*) scheduler_instance->priv;

        if(scheduler) {
            continue;
        }

        retval = qos_scheduler_new(&scheduler, scheduler_instance);
        when_failed(retval, exit);
        retval = qos_dm_create_node(scheduler_instance);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static int qos_cleanup_schedulers(const qos_t* const qos) {
    int retval = -1;
    amxd_object_t* scheduler_object = NULL;

    when_null(qos, exit);

    scheduler_object = qos_get_object(QOS_TR181_DEVICE_QOS_SCHEDULER_PATH);
    when_null_status(scheduler_object, exit, retval = 0);

    amxd_object_for_each(instance, it, scheduler_object) {
        amxd_object_t* scheduler_instance = amxc_container_of(it, amxd_object_t, it);
        qos_scheduler_t* scheduler = (qos_scheduler_t*) scheduler_instance->priv;

        if(!scheduler) {
            continue;
        }

        netmodel_closeQuery(scheduler->query);
        scheduler->query = NULL;

        retval = qos_scheduler_delete(&scheduler);
        when_failed(retval, exit);
    }

    retval = 0;

exit:
    return retval;
}

static void qos_activate_or_deactivate_children(qos_node_t* node, bool activate) {
    amxc_llist_t* node_list = NULL;
    when_null(node, exit);
    node_list = qos_node_get_children(node);
    when_null(node_list, exit);
    amxc_llist_for_each(iter, node_list) {
        qos_node_t* child_node = qos_node_llist_it_get_node(iter);
        if(!child_node) {
            continue;
        }
        if(!activate) {
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_QUEUE) {
                qos_queue_deactivate(child_node->data.queue);
            }
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_SHAPER) {
                qos_shaper_deactivate(child_node->data.shaper);
            }
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_SCHEDULER) {
                qos_scheduler_deactivate(child_node->data.scheduler);
            }
        } else {
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_QUEUE) {
                if(qos_queue_dm_get_enable(child_node->data.queue)) {
                    qos_queue_activate(child_node->data.queue);
                }
            }
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_SHAPER) {
                if(qos_shaper_dm_get_enable(child_node->data.shaper)) {
                    qos_shaper_activate(child_node->data.shaper);
                }
            }
            if(qos_node_get_type(child_node) == QOS_NODE_TYPE_SCHEDULER) {
                if(qos_scheduler_dm_get_enable(child_node->data.scheduler)) {
                    qos_scheduler_activate(child_node->data.scheduler);
                }
            }

        }
    }
exit:
    return;
}

int qos_new(qos_t** qos, amxd_object_t* qos_instance) {
    int retval = -1;

    when_null(qos, exit);
    when_null(qos_instance, exit);
    when_not_null(*qos, exit);
    when_not_null((qos_t*) qos_instance->priv, exit);

    *qos = (qos_t*) calloc(1, sizeof(qos_t));
    when_null(*qos, exit);

    retval = qos_init(*qos, qos_instance);

exit:
    if(retval != 0) {
        qos_delete(qos);
    }
    return retval;
}

int qos_delete(qos_t** qos) {
    int retval = -1;

    when_null(qos, exit);
    when_null(*qos, exit);

    qos_cleanup(*qos);

    free(*qos);
    *qos = NULL;
    retval = 0;

exit:
    return retval;
}

//! [qos_init]
int qos_init(qos_t* const qos, amxd_object_t* const qos_instance) {
    int retval = -1;

    when_null(qos, exit);
    when_null(qos_instance, exit);

    qos->dm_object = qos_instance;

    retval = qos_common_init(qos);
    when_failed(retval, exit);

    retval = qos_module_mngr_init();
    when_failed(retval, exit);

    retval = qos_create_shapers(qos);
    when_failed(retval, exit);

    retval = qos_create_schedulers(qos);
    when_failed(retval, exit);

    retval = qos_create_queues(qos);
    when_failed(retval, exit);

    retval = qos_create_queue_stats(qos);
    when_failed(retval, exit);

    retval = qos_dm_create_nodes_relations(qos);
    when_failed(retval, exit);

    retval = qos_create_nodes(qos);
    when_failed(retval, exit);

    retval = qos_create_classifications(qos);
    when_failed(retval, exit);

    qos_instance->priv = (void*) qos;
    retval = 0;

exit:
    return retval;
}
//! [qos_init]

int qos_cleanup(qos_t* const qos) {
    int retval = -1;

    retval = qos_cleanup_classifications(qos);
    when_failed(retval, exit);

    retval = qos_cleanup_nodes(qos);
    when_failed(retval, exit);

    retval = qos_cleanup_queue_stats(qos);
    when_failed(retval, exit);

    retval = qos_cleanup_queues(qos);
    when_failed(retval, exit);

    retval = qos_cleanup_schedulers(qos);
    when_failed(retval, exit);

    retval = qos_cleanup_shapers(qos);
    when_failed(retval, exit);

exit:
    qos_common_cleanup();

    return retval;
}

qos_t* qos_get_dm_object_priv_data(const amxd_object_t* const object) {
    qos_t* qos_priv = NULL;

    when_null(object, exit);
    qos_priv = (qos_t*) object->priv;

exit:
    return qos_priv;
}

amxd_object_t* qos_get_dm_object(const qos_t* const qos) {
    amxd_object_t* object = NULL;

    when_null(qos, exit);
    object = qos->dm_object;

exit:
    return object;
}

uint32_t qos_dm_get_max_classification_entries(const qos_t* const qos) {
    uint32_t max_classification_entries = 0;
    amxd_object_t* object = NULL;
    const char* param = "MaxClassificationEntries";

    when_null(qos, exit);

    object = qos_get_dm_object(qos);
    when_null(object, exit);

    max_classification_entries = amxd_object_get_value(uint32_t, object, param, NULL);

exit:
    return max_classification_entries;
}

uint32_t qos_dm_get_max_queue_entries(const qos_t* const qos) {
    uint32_t max_queue_entries = 0;
    amxd_object_t* object = NULL;
    const char* param = "MaxQueueEntries";

    when_null(qos, exit);

    object = qos_get_dm_object(qos);
    when_null(object, exit);

    max_queue_entries = amxd_object_get_value(uint32_t, object, param, NULL);

exit:
    return max_queue_entries;
}

const char* qos_status_to_string(const qos_status_t status) {
    const char* str_status[] = {
        "Disabled", "Enabled", "Error_Misconfigured", "Error"
    };

    if(status >= QOS_STATUS_LAST) {
        return "Unknown";
    }
    return str_status[status];
}

qos_status_t qos_status_from_string(const char* status) {
    qos_status_t retval = QOS_STATUS_DISABLED;
    when_str_empty(status, exit);

    for(uint32_t i = 0; i < QOS_STATUS_LAST; i++) {
        const char* str_status = qos_status_to_string((qos_status_t) i);
        if(!strncmp(status, str_status, strlen(str_status))) {
            retval = (qos_status_t) i;
            break;
        }
    }
exit:
    return retval;
}

void qos_activate_children(qos_node_t* node) {
    qos_activate_or_deactivate_children(node, true);
}

void qos_deactivate_children(qos_node_t* node) {
    qos_activate_or_deactivate_children(node, false);
}

