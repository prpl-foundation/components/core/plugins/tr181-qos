/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2023 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <setjmp.h>
#include <cmocka.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxo/amxo.h>

#include <amxut/amxut_macros.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "tr181-qos.h"
#include "qos.h"
#include "dm_tr181-qos.h"
#include "dm-qos-classification.h"
#include "dm-qos-node.h"
#include "dm-qos-queue.h"
#include "dm-qos-queue-stats.h"
#include "dm-qos-scheduler.h"
#include "dm-qos-shaper.h"

#include "test_common.h"

static void test_common_resolve_all_functions(void) {
    amxut_resolve_function("qos_main", _qos_main);
    amxut_resolve_function("dm_qos_classification_evt_instance_added", _dm_qos_classification_evt_instance_added);
    amxut_resolve_function("dm_qos_classification_evt_instance_changed", _dm_qos_classification_evt_instance_changed);
    amxut_resolve_function("dm_qos_classification_action_on_destroy", _dm_qos_classification_action_on_destroy);
    amxut_resolve_function("dm_qos_classification_action_validate", _dm_qos_classification_action_validate);
    amxut_resolve_function("dm_qos_queue_action_on_read", _dm_qos_queue_action_on_read);
    amxut_resolve_function("dm_qos_queue_stats_action_on_read", _dm_qos_queue_stats_action_on_read);
    amxut_resolve_function("dm_qos_queue_action_on_delete", _dm_qos_queue_action_on_delete);
    amxut_resolve_function("dm_qos_queue_action_on_destroy", _dm_qos_queue_action_on_destroy);
    amxut_resolve_function("dm_qos_queue_evt_instance_added", _dm_qos_queue_evt_instance_added);
    amxut_resolve_function("dm_qos_queue_evt_instance_changed", _dm_qos_queue_evt_instance_changed);
    amxut_resolve_function("dm_qos_queue_stats_action_on_delete", _dm_qos_queue_stats_action_on_delete);
    amxut_resolve_function("dm_qos_queue_stats_action_on_destroy", _dm_qos_queue_stats_action_on_destroy);
    amxut_resolve_function("dm_qos_queue_stats_evt_instance_added", _dm_qos_queue_stats_evt_instance_added);
    amxut_resolve_function("dm_qos_queue_stats_evt_instance_changed", _dm_qos_queue_stats_evt_instance_changed);
    amxut_resolve_function("dm_qos_shaper_action_on_delete", _dm_qos_shaper_action_on_delete);
    amxut_resolve_function("dm_qos_shaper_action_on_destroy", _dm_qos_shaper_action_on_destroy);
    amxut_resolve_function("dm_qos_shaper_evt_instance_added", _dm_qos_shaper_evt_instance_added);
    amxut_resolve_function("dm_qos_shaper_evt_instance_changed", _dm_qos_shaper_evt_instance_changed);
    amxut_resolve_function("dm_qos_scheduler_action_on_destroy", _dm_qos_scheduler_action_on_destroy);
    amxut_resolve_function("dm_qos_scheduler_evt_instance_added", _dm_qos_scheduler_evt_instance_added);
    amxut_resolve_function("dm_qos_scheduler_evt_instance_changed", _dm_qos_scheduler_evt_instance_changed);
    amxut_resolve_function("queue_flags_changed", _queue_flags_changed);
}

int test_qos_common_setup(void** state) {
    static const char* odl_common = "../common/test_common.odl";

    amxut_bus_setup(state);

    test_common_resolve_all_functions();
    amxut_dm_load_odl(odl_common);

    assert_int_equal(_qos_main(0, amxut_bus_dm(), amxut_bus_parser()), 0);
    _qos_app_start(NULL, NULL, NULL);
    amxut_bus_handle_events();

    return 0;
}

int test_qos_common_teardown(void** state) {
    assert_int_equal(_qos_main(1, amxut_bus_dm(), amxut_bus_parser()), 0);
    amxut_bus_teardown(state);

    return 0;
}

int test_variant_equal_check(const LargestIntegralType value, const LargestIntegralType check_value_data) {
    amxc_var_t* actual_variant = (amxc_var_t*) value;
    amxc_var_t* expected_variant = (amxc_var_t*) check_value_data;
    int result = 0;

    printf("%s\nExpected variant:\n", __func__);
    fflush(stdout);
    amxc_var_dump(expected_variant, STDOUT_FILENO);
    printf("Actual variant:\n");
    fflush(stdout);
    amxc_var_dump(actual_variant, STDOUT_FILENO);

    assert_int_equal(amxc_var_compare(expected_variant, actual_variant, &result), 0);
    assert_int_equal(result, 0);
    printf("\n");

    amxc_var_delete(&expected_variant);
    return 1;
}
