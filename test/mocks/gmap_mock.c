/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxo/amxo.h>

#include <amxut/amxut_macros.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "gmap_mock.h"

static gmap_query_cb_t gmap_device_cb;

static int add_ip_addr(UNUSED amxd_object_t* object, amxd_object_t* mobject, void* priv) {
    amxc_var_t* ip_list = (amxc_var_t*) priv;
    amxc_var_t* ip_entry = amxc_var_add(amxc_htable_t, ip_list, NULL);
    amxc_var_t* address = amxc_var_add_new_key(ip_entry, "Address");
    char* ip = amxd_object_get_value(cstring_t, mobject, "Address", NULL);
    amxc_var_push(cstring_t, address, ip);

    return 0;
}

void gmap_mock_callback_set(gmap_query_cb_t cb) {
    gmap_device_cb = cb;
}

gmap_query_cb_t gmap_mock_callback_get(void) {
    return gmap_device_cb;
}

amxc_var_t* gmap_mock_device_params(const char* alias) {
    amxc_var_t* retval = NULL;
    amxc_var_t* ip_addr = NULL;
    amxd_object_t* object = NULL;
    char* phys_addr = NULL;

    amxc_var_new(&retval);
    amxc_var_set_type(retval, AMXC_VAR_ID_HTABLE);

    object = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.", alias);
    assert_non_null(object);

    ip_addr = amxc_var_add_key(amxc_llist_t, retval, "IPv4Address", NULL);
    amxd_object_for_all(object, "IPv4Address.*.", add_ip_addr, ip_addr);

    ip_addr = amxc_var_add_key(amxc_llist_t, retval, "IPv6Address", NULL);
    amxd_object_for_all(object, "IPv6Address.*.", add_ip_addr, ip_addr);

    phys_addr = amxd_object_get_value(cstring_t, object, "PhysAddress", NULL);
    if(phys_addr != NULL) {
        amxc_var_add_key(cstring_t, retval, "PhysAddress", phys_addr);
    }

    free(phys_addr);
    return retval;
}

int gmap_mock_add_ip_addr(const char* alias, uint32_t ip_version, const char* ip_addr) {
    int retval = -1;
    amxd_trans_t trans;

    amxd_trans_init(&trans);
    if(ip_version == 4) {
        amxd_trans_select_pathf(&trans, "Devices.Device.%s.IPv4Address", alias);
    } else {
        amxd_trans_select_pathf(&trans, "Devices.Device.%s.IPv6Address", alias);
    }
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(cstring_t, &trans, "Address", ip_addr);
    retval = amxd_trans_apply(&trans, amxut_bus_dm());

    amxd_trans_clean(&trans);
    return retval;
}

int gmap_mock_del_ip_addr(const char* alias, uint32_t ip_version, const char* ip_addr) {
    int retval = -1;
    amxc_var_t ret;
    amxc_string_t object;

    amxc_var_init(&ret);
    amxc_string_init(&object, 0);

    if(ip_version == 4) {
        amxc_string_setf(&object, "Devices.Device.%s.IPv4Address.[Address == '%s'].", alias, ip_addr);
    } else {
        amxc_string_setf(&object, "Devices.Device.%s.IPv6Address.[Address == '%s'].", alias, ip_addr);
    }

    retval = amxb_del(amxut_bus_ctx(), amxc_string_get(&object, 0), 0, NULL, &ret, 5);

    amxc_string_clean(&object);
    amxc_var_clean(&ret);
    return retval;
}

int gmap_mock_clear_ip_addrs(const char* alias, uint32_t ip_version) {
    int retval = -1;
    amxc_string_t object;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_string_init(&object, 0);
    amxc_string_setf(&object, "Devices.Device.%s.%s.*.", alias, ip_version == 4 ? "IPv4Address" : "IPv6Address");

    retval = amxb_del(amxut_bus_ctx(), amxc_string_get(&object, 0), 0, NULL, &ret, 5);

    amxc_string_clean(&object);
    amxc_var_clean(&ret);
    return retval;
}

void __wrap_gmap_client_init(UNUSED amxb_bus_ctx_t* bus_ctx) {
    return;
}

gmap_query_t* __wrap_gmap_query_open_ext(const char* expression, UNUSED const char* name, gmap_query_cb_t fn, void* user_data) {
    gmap_query_t* query = NULL;

    check_expected(expression);

    query = (gmap_query_t*) calloc(1, sizeof(gmap_query_t));
    query->data = user_data;
    gmap_mock_callback_set(fn);

    return query;
}

void __wrap_gmap_query_close(gmap_query_t* query) {
    free(query);
}
