/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2024 SoftAtHome
**
** Redistribution and use in source and binary forms, with or without modification,
** are permitted provided that the following conditions are met:
**
** 1. Redistributions of source code must retain the above copyright notice,
** this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above copyright notice,
** this list of conditions and the following disclaimer in the documentation
** and/or other materials provided with the distribution.
**
** Subject to the terms and conditions of this license, each copyright holder
** and contributor hereby grants to those receiving rights under this license
** a perpetual, worldwide, non-exclusive, no-charge, royalty-free, irrevocable
** (except for failure to satisfy the conditions of this license) patent license
** to make, have made, use, offer to sell, sell, import, and otherwise transfer
** this software, where such license applies only to those patent claims, already
** acquired or hereafter acquired, licensable by such copyright holder or contributor
** that are necessarily infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright holders and
** non-copyrightable additions of contributors, in source or binary form) alone;
** or
**
** (b) combination of their Contribution(s) with the work of authorship to which
** such Contribution(s) was added by such copyright holder or contributor, if,
** at the time the Contribution is added, such addition causes such combination
** to be necessarily infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any copyright
** holder or contributor is granted under this license, whether expressly, by
** implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
** AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
** IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
** ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
** LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
** DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
** SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
** CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
** OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
** USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#ifndef _GNU_SOURCE
#define _GNU_SOURCE
#endif

#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>
#include <string.h>
#include <dlfcn.h>

#include <amxc/amxc.h>
#include <amxp/amxp.h>
#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>
#include <amxo/amxo.h>

#include <amxut/amxut_macros.h>
#include <amxut/amxut_bus.h>
#include <amxut/amxut_dm.h>

#include "tr181-qos.h"
#include "qos.h"
#include "dm_tr181-qos.h"
#include "dm-qos-classification.h"
#include "qos-classification.h"
#include "qos-classification-device.h"

#include "test_qos_classification_device.h"
#include "test_common.h"
#include "gmap_mock.h"

/**
 * Add a classification instance to the data model
 * Classification criterion can be one of (Source/Dest)VendorClassID/UserClassID
 * Mode can be Exact (default), Prefix, Suffix or Substring
 * IP version can be 4 or 6
 */
static uint32_t classification_instance_add(const char* classification_criterion, const char* mode, bool exclude, uint32_t ip_version) {
    uint32_t index = 0;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DSCPMark", 56);
    amxd_trans_set_value(cstring_t, &trans, classification_criterion, "test");
    if((mode != NULL) && (*mode != 0)) {
        amxc_string_t mode_param;
        amxc_string_init(&mode_param, 0);
        amxc_string_setf(&mode_param, "%sMode", classification_criterion);
        amxd_trans_set_value(cstring_t, &trans, amxc_string_get(&mode_param, 0), mode);
        amxc_string_clean(&mode_param);
    }
    if(exclude) {
        amxc_string_t exclude_param;
        amxc_string_init(&exclude_param, 0);
        amxc_string_setf(&exclude_param, "%sExclude", classification_criterion);
        amxd_trans_set_value(bool, &trans, amxc_string_get(&exclude_param, 0), exclude);
        amxc_string_clean(&exclude_param);
    }
    if(ip_version == 6) {
        amxd_trans_set_value(cstring_t, &trans, "DHCPType", "DHCPv6");
    }
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    index = GETP_UINT32(&trans.retvals, "1.index");

    amxd_trans_clean(&trans);
    return index;
}

static void classification_instance_delete(uint32_t index) {
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_del_inst(&trans, index, NULL);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    amxd_trans_clean(&trans);
}

static void device_based_classification_common_start(uint32_t class_index, bool source, const char* gmap_alias) {
    amxd_object_t* object = NULL;
    amxc_var_t* device_params = NULL;
    gmap_query_cb_t cb = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;
    int feature_marks = 0;
    int feature = source ? FW_FEATURE_SOURCE : FW_FEATURE_DESTINATION;
    gmap_query_t* query = NULL;

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", class_index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    // After a classification instance is added, a gmap query will be opened. We assume gmap works
    // fine, so we will just call the callback function ourselves
    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    // Get the device parameters from the gmap data model to pass them to the gmap callback
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    query = source ? class_device->query_src : class_device->query_dst;

    // Check the feature marks
    // Before calling the callback, the mark is not set, afterwards it must be set
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_false((feature_marks & (1 << feature)) > 0);
    cb(query, gmap_alias, device_params, gmap_query_expression_start_matching);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    amxc_var_delete(&device_params);
}

// To make a device stop matching the gmap expression, we also need to call the callback function
// manually.
static void device_based_classification_common_stop(uint32_t index, bool source, const char* gmap_alias) {
    amxd_object_t* object = NULL;
    gmap_query_cb_t cb = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;
    gmap_query_t* query = NULL;

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    query = source ? class_device->query_src : class_device->query_dst;
    cb(query, gmap_alias, NULL, gmap_query_expression_stop_matching);
}

static int test_check_device_rule_size(qos_classification_device_t* class_device, bool src) {
    amxc_var_t* device_rules = NULL;
    int count = 0;

    if(src) {
        device_rules = qos_classification_device_get_device_rules_src(class_device);
    } else {
        device_rules = qos_classification_device_get_device_rules_dst(class_device);
    }

    amxc_var_for_each(device, device_rules) {
        amxc_var_for_each(ip, device) {
            count++;
        }
    }

    return count;
}

int test_qos_classification_device_setup(void** state) {
    const char* gmap_odl = "gmap-server.odl";

    test_qos_common_setup(state);
    amxut_dm_load_odl(gmap_odl);
    _qos_classification_device_entrypoint(0, NULL, NULL);
    return 0;
}

int test_qos_classification_device_teardown(void** state) {
    return test_qos_common_teardown(state);
}

void test_can_add_source_vendor_class_id_classification(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_source_vendor_class_id_classification_exclude(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, true, 4);
    device_based_classification_common_start(index, true, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_source_vendor_class_id_classification_mode(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID starts with 'test')");
    index = classification_instance_add("SourceVendorClassID", "Prefix", false, 4);
    device_based_classification_common_start(index, true, "single-ip");
    classification_instance_delete(index);

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID ends with 'test')");
    index = classification_instance_add("SourceVendorClassID", "Suffix", false, 4);
    device_based_classification_common_start(index, true, "single-ip");
    classification_instance_delete(index);

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID matches 'test')");
    index = classification_instance_add("SourceVendorClassID", "Substring", false, 4);
    device_based_classification_common_start(index, true, "single-ip");
    classification_instance_delete(index);
}

void test_can_add_source_vendor_class_id_classification_v6(UNUSED void** state) {
    uint32_t index = 0;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    // Note that we provide a Prefix, but we still expect an exact match for DHCPv6,
    // so we're also testing that the Mode parameter is ignored
    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassIDv6 == 'test')");

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DSCPMark", 56);
    amxd_trans_set_value(cstring_t, &trans, "DHCPType", "DHCPv6");
    amxd_trans_set_value(cstring_t, &trans, "SourceVendorClassIDv6", "test");
    amxd_trans_set_value(cstring_t, &trans, "SourceVendorClassIDMode", "Prefix");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    index = GETP_UINT32(&trans.retvals, "1.index");

    device_based_classification_common_start(index, true, "single-ip");
    classification_instance_delete(index);

    amxd_trans_clean(&trans);
}

void test_can_add_source_vendor_class_id_classification_v6_exclude(UNUSED void** state) {
    uint32_t index = 0;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.VendorClassIDv6 == 'test')");

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DSCPMark", 56);
    amxd_trans_set_value(cstring_t, &trans, "DHCPType", "DHCPv6");
    amxd_trans_set_value(cstring_t, &trans, "SourceVendorClassIDv6", "test");
    amxd_trans_set_value(bool, &trans, "SourceVendorClassIDExclude", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    index = GETP_UINT32(&trans.retvals, "1.index");

    device_based_classification_common_start(index, true, "single-ip");
    classification_instance_delete(index);

    amxd_trans_clean(&trans);
}

void test_can_add_dest_vendor_class_id_classification(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("DestVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, false, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_dest_vendor_class_id_classification_exclude(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.VendorClassID == 'test')");
    index = classification_instance_add("DestVendorClassID", NULL, true, 4);
    device_based_classification_common_start(index, false, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_dest_vendor_class_id_classification_mode(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID starts with 'test')");
    index = classification_instance_add("DestVendorClassID", "Prefix", false, 4);
    device_based_classification_common_start(index, false, "single-ip");
    classification_instance_delete(index);

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID ends with 'test')");
    index = classification_instance_add("DestVendorClassID", "Suffix", false, 4);
    device_based_classification_common_start(index, false, "single-ip");
    classification_instance_delete(index);

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID matches 'test')");
    index = classification_instance_add("DestVendorClassID", "Substring", false, 4);
    device_based_classification_common_start(index, false, "single-ip");
    classification_instance_delete(index);
}

void test_can_add_dest_vendor_class_id_classification_v6(UNUSED void** state) {
    uint32_t index = 0;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    // Note that we provide a Prefix, but we still expect an exact match for DHCPv6,
    // so we're also testing that the Mode parameter is ignored
    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassIDv6 == 'test')");

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DSCPMark", 56);
    amxd_trans_set_value(cstring_t, &trans, "DHCPType", "DHCPv6");
    amxd_trans_set_value(cstring_t, &trans, "DestVendorClassIDv6", "test");
    amxd_trans_set_value(cstring_t, &trans, "DestVendorClassIDMode", "Prefix");
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    index = GETP_UINT32(&trans.retvals, "1.index");

    device_based_classification_common_start(index, false, "single-ip");
    classification_instance_delete(index);

    amxd_trans_clean(&trans);
}

void test_can_add_dest_vendor_class_id_classification_v6_exclude(UNUSED void** state) {
    uint32_t index = 0;
    amxd_trans_t trans;

    amxd_trans_init(&trans);

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.VendorClassIDv6 == 'test')");

    amxd_trans_select_pathf(&trans, "QoS.Classification.");
    amxd_trans_add_inst(&trans, 0, NULL);
    amxd_trans_set_value(bool, &trans, "Enable", true);
    amxd_trans_set_value(int32_t, &trans, "DSCPMark", 56);
    amxd_trans_set_value(cstring_t, &trans, "DHCPType", "DHCPv6");
    amxd_trans_set_value(cstring_t, &trans, "DestVendorClassIDv6", "test");
    amxd_trans_set_value(bool, &trans, "DestVendorClassIDExclude", true);
    assert_int_equal(amxd_trans_apply(&trans, amxut_bus_dm()), 0);
    amxut_bus_handle_events();

    index = GETP_UINT32(&trans.retvals, "1.index");

    device_based_classification_common_start(index, false, "single-ip");
    classification_instance_delete(index);

    amxd_trans_clean(&trans);
}

void test_can_add_source_user_class_id_classification(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.UserClassID == 'test')");
    index = classification_instance_add("SourceUserClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_source_user_class_id_classification_exclude(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.UserClassID == 'test')");
    index = classification_instance_add("SourceUserClassID", NULL, true, 4);
    device_based_classification_common_start(index, true, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_dest_user_class_id_classification(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.UserClassID == 'test')");
    index = classification_instance_add("DestUserClassID", NULL, false, 4);
    device_based_classification_common_start(index, false, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_dest_user_class_id_classification_exclude(UNUSED void** state) {
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "!(.UserClassID == 'test')");
    index = classification_instance_add("DestUserClassID", NULL, true, 4);
    device_based_classification_common_start(index, false, "single-ip");

    classification_instance_delete(index);
}

void test_can_add_multi_ip_device_based_classification(UNUSED void** state) {
    amxd_object_t* object = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;
    uint32_t index = 0;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, "multi-ip");

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    // 1 rule was added for each IP
    assert_int_equal(test_check_device_rule_size(class_device, true), 2);

    classification_instance_delete(index);
}

void test_classification_src_can_stop_matching(UNUSED void** state) {
    uint32_t index = 0;
    const char* gmap_alias = "single-ip";

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, gmap_alias);
    device_based_classification_common_stop(index, true, gmap_alias);

    classification_instance_delete(index);
}

void test_classification_dst_can_stop_matching(UNUSED void** state) {
    uint32_t index = 0;
    const char* gmap_alias = "single-ip";

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("DestVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, false, gmap_alias);
    device_based_classification_common_stop(index, false, gmap_alias);

    classification_instance_delete(index);
}

static void test_device_ips_can_be_updated(bool source) {
    uint32_t index = 0;
    const char* gmap_alias = "single-ip";
    const char* ip1 = "192.168.1.10";
    const char* ip2 = "192.168.1.11";
    amxd_object_t* object = NULL;
    amxc_var_t* device_params = NULL;
    gmap_query_cb_t cb = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;
    int feature_marks = 0;
    int feature = source ? FW_FEATURE_SOURCE : FW_FEATURE_DESTINATION;
    gmap_query_t* query = NULL;

    // Clear existing IP addresses
    gmap_mock_clear_ip_addrs(gmap_alias, 4);
    amxut_bus_handle_events();

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    if(source) {
        index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    } else {
        index = classification_instance_add("DestVendorClassID", NULL, false, 4);
    }

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    // After a classification instance is added, a gmap query will be opened. We assume gmap works
    // fine, so we will just call the callback function ourselves
    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    // Get the device parameters from the gmap data model to pass them to the gmap callback
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    query = source ? class_device->query_src : class_device->query_dst;

    // Check the feature marks
    // They are still unset after calling the gmap callback, because the device doesn't have an IP yet
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_false((feature_marks & (1 << feature)) > 0);
    cb(query, gmap_alias, device_params, gmap_query_expression_start_matching);

    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_false((feature_marks & (1 << feature)) > 0);

    // No rules were added either
    assert_int_equal(test_check_device_rule_size(class_device, source), 0);

    // Now add an IP address
    gmap_mock_add_ip_addr(gmap_alias, 4, ip1);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    // Check that a rule was added
    assert_int_equal(test_check_device_rule_size(class_device, source), 1);

    // Add a second IP address
    gmap_mock_add_ip_addr(gmap_alias, 4, ip2);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    // Check that a rule was added
    assert_int_equal(test_check_device_rule_size(class_device, source), 2);

    // Delete the last IP address that was added
    gmap_mock_del_ip_addr(gmap_alias, 4, ip2);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    // Check that a rule was deleted
    assert_int_equal(test_check_device_rule_size(class_device, source), 1);

    // Add the IP address back
    gmap_mock_add_ip_addr(gmap_alias, 4, ip2);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    // Check that a rule was added
    assert_int_equal(test_check_device_rule_size(class_device, source), 2);

    // Delete the first IP instead
    gmap_mock_del_ip_addr(gmap_alias, 4, ip1);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    assert_true((feature_marks & (1 << feature)) > 0);

    // Check that a rule was deleted
    assert_int_equal(test_check_device_rule_size(class_device, source), 1);

    // Remove all IPs
    gmap_mock_clear_ip_addrs(gmap_alias, 4);
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(query, gmap_alias, device_params, gmap_query_device_updated);
    feature_marks = fw_folder_get_feature_marks(qos_classification_get_folder(classification));
    // Feature mark is now unset
    assert_false((feature_marks & (1 << feature)) > 0);

    // Check that a rule was deleted
    assert_int_equal(test_check_device_rule_size(class_device, source), 0);

    classification_instance_delete(index);
    amxc_var_delete(&device_params);
}

void test_device_ips_can_be_updated_src(UNUSED void** state) {
    test_device_ips_can_be_updated(true);
}

void test_device_ips_can_be_updated_dst(UNUSED void** state) {
    test_device_ips_can_be_updated(false);
}

// Initially match with first gmap device, then add second device, then remove an IP
void test_classification_multiple_devices(UNUSED void** state) {
    uint32_t index = 0;
    const char* gmap_alias1 = "single-ip";
    const char* gmap_alias2 = "multi-ip";
    amxd_object_t* object = NULL;
    amxc_var_t* device_params = NULL;
    gmap_query_cb_t cb = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;

    // Add an IP address
    gmap_mock_add_ip_addr(gmap_alias1, 4, "192.168.1.100");
    amxut_bus_handle_events();

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, gmap_alias1);

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    // There should be 1 IP in the list
    assert_int_equal(test_check_device_rule_size(class_device, true), 1);

    // Now start matching with 2nd device
    // Get the device parameters from the gmap data model to pass them to the gmap callback
    device_params = gmap_mock_device_params(gmap_alias2);
    assert_non_null(device_params);

    // Get gmap cb function
    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    cb(class_device->query_src, gmap_alias2, device_params, gmap_query_device_updated);

    // There should now be 3 IPs in the list
    assert_int_equal(test_check_device_rule_size(class_device, true), 3);

    // Now delete one of the IPs from gmap instance 2
    gmap_mock_del_ip_addr(gmap_alias2, 4, "192.168.1.11");
    amxut_bus_handle_events();

    // Fetch new device params
    amxc_var_delete(&device_params);
    device_params = gmap_mock_device_params(gmap_alias2);
    assert_non_null(device_params);

    // Call callback function with action updated and check feature marks
    cb(class_device->query_src, gmap_alias2, device_params, gmap_query_device_updated);

    // Check that a rule was deleted
    assert_int_equal(test_check_device_rule_size(class_device, true), 2);

    classification_instance_delete(index);
    amxc_var_delete(&device_params);
}

// Test that gmap_query_error does nothing
static void test_classification_gmap_query_error(const char* param, bool source) {
    uint32_t index = 0;
    const char* gmap_alias = "single-ip";
    amxd_object_t* object = NULL;
    amxc_var_t* device_params = NULL;
    gmap_query_cb_t cb = NULL;
    gmap_query_t* query = NULL;
    qos_classification_t* classification = NULL;
    qos_classification_device_t* class_device = NULL;

    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add(param, NULL, false, 4);
    device_based_classification_common_start(index, source, gmap_alias);

    object = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index);
    assert_non_null(object);

    classification = (qos_classification_t*) object->priv;
    assert_non_null(classification);

    class_device = qos_classification_device_get_class_device(classification);
    assert_non_null(class_device);

    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Get gmap cb function
    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    query = source ? class_device->query_src : class_device->query_dst;
    cb(query, gmap_alias, device_params, gmap_query_device_updated);

    // There should be 1 IP in the list
    assert_int_equal(test_check_device_rule_size(class_device, source), 1);

    // Callback with case error
    cb(query, gmap_alias, device_params, gmap_query_error);

    // There should still be 1 IP in the list
    assert_int_equal(test_check_device_rule_size(class_device, source), 1);

    amxc_var_delete(&device_params);
    classification_instance_delete(index);
}

void test_classification_gmap_query_error_src(UNUSED void** state) {
    test_classification_gmap_query_error("SourceVendorClassID", true);
}

void test_classification_gmap_query_error_dst(UNUSED void** state) {
    test_classification_gmap_query_error("DestVendorClassID", false);
}

static void test_classification_device_action_signal_cb(UNUSED const char* const sig_name,
                                                        const amxc_var_t* const data,
                                                        UNUSED void* const priv) {
    check_expected(data);
}

static void test_classification_device_action_signal_set_expected_variant(const char* gmap_alias, uint32_t gmap_action) {
    amxc_var_t* expected_variant = NULL;

    amxc_var_new(&expected_variant);
    amxc_var_set_type(expected_variant, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(uint32_t, expected_variant, "action", gmap_action);
    amxc_var_add_key(cstring_t, expected_variant, "alias", gmap_alias);
    amxc_var_add_key(int32_t, expected_variant, "EthernetPriorityMark", -1);

    // PhysAddress is not provided when the device stops matching
    if(gmap_action == gmap_query_expression_start_matching) {
        amxd_object_t* object = amxd_dm_findf(amxut_bus_dm(), "Devices.Device.%s.", gmap_alias);
        char* phys_addr = amxd_object_get_value(cstring_t, object, "PhysAddress", NULL);
        amxc_var_add_key(cstring_t, expected_variant, "PhysAddress", phys_addr);
        free(phys_addr);
    }

    expect_check(test_classification_device_action_signal_cb, data, test_variant_equal_check, expected_variant);
}

// Test to make sure signal SIG_DEVICE_UPDATED is triggered with the right data for the expected device
void test_classification_triggers_device_action_signal(UNUSED void** state) {
    uint32_t index = 0;
    amxp_signal_t* sig = NULL;
    const char* gmap_alias = "single-ip";

    amxp_sigmngr_add_signal(NULL, SIG_DEVICE_UPDATED);
    amxp_slot_connect(NULL, SIG_DEVICE_UPDATED, NULL, test_classification_device_action_signal_cb, NULL);

    // Signal is triggered when classification instance is added and gmap query callback is called
    test_classification_device_action_signal_set_expected_variant(gmap_alias, gmap_query_expression_start_matching);
    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index = classification_instance_add("SourceVendorClassID", NULL, false, 4);
    device_based_classification_common_start(index, true, gmap_alias);

    // Signal is also triggered when classfication instance is removed
    test_classification_device_action_signal_set_expected_variant(gmap_alias, gmap_query_expression_stop_matching);
    classification_instance_delete(index);

    sig = amxp_sigmngr_find_signal(NULL, SIG_DEVICE_UPDATED);
    amxp_sigmngr_remove_signal(NULL, SIG_DEVICE_UPDATED);
    amxp_signal_delete(&sig);
}

// There can be several classification instances that create the same gmap query. The real query
// will only be created once and when a device is updated, the callback function will be called
// multiple times with the same device data variant. There was an issue where the device data
// variant was modified the first time the callback function was called. This test ensures that this
// is no longer an issue.
void test_classification_gmap_double_callback(UNUSED void** state) {
    const char* gmap_alias = "single-ip";
    const char* param = "SourceVendorClassID";
    bool source = true;
    amxc_var_t* device_params = NULL;
    gmap_query_cb_t cb = NULL;
    uint32_t index1 = 0;
    uint32_t index2 = 0;
    amxd_object_t* object1 = NULL;
    amxd_object_t* object2 = NULL;
    qos_classification_t* classification1 = NULL;
    qos_classification_t* classification2 = NULL;
    qos_classification_device_t* class_device1 = NULL;
    qos_classification_device_t* class_device2 = NULL;
    gmap_query_t* query1 = NULL;
    gmap_query_t* query2 = NULL;

    // Add first classification instance
    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index1 = classification_instance_add(param, NULL, false, 4);
    device_based_classification_common_start(index1, source, gmap_alias);

    // Add second classification instance
    expect_string(__wrap_gmap_query_open_ext, expression, "(.VendorClassID == 'test')");
    index2 = classification_instance_add(param, NULL, false, 4);
    device_based_classification_common_start(index2, source, gmap_alias);

    // Find first classification object
    object1 = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index1);
    assert_non_null(object1);

    // Find second classification object
    object2 = amxd_dm_findf(amxut_bus_dm(), "QoS.Classification.%d.", index2);
    assert_non_null(object2);

    // Find first classification data
    classification1 = (qos_classification_t*) object1->priv;
    assert_non_null(classification1);

    // Find second classification data
    classification2 = (qos_classification_t*) object2->priv;
    assert_non_null(classification2);

    // Find first classification device data
    class_device1 = qos_classification_device_get_class_device(classification1);
    assert_non_null(class_device1);

    // Find second classification device data
    class_device2 = qos_classification_device_get_class_device(classification2);
    assert_non_null(class_device2);

    // Find first gmap query
    query1 = source ? class_device1->query_src : class_device1->query_dst;

    // Find second gmap query
    query2 = source ? class_device2->query_src : class_device2->query_dst;

    // Device params are generic for both
    device_params = gmap_mock_device_params(gmap_alias);
    assert_non_null(device_params);

    // Get gmap cb function that will be called for each of the classifications
    cb = gmap_mock_callback_get();
    assert_non_null(cb);

    // Call cb function for first query with device_params
    cb(query1, gmap_alias, device_params, gmap_query_device_updated);

    // Call cb function for second query with the same device_params
    cb(query2, gmap_alias, device_params, gmap_query_device_updated);

    // There should be 1 IP in the list for both classifications
    assert_int_equal(test_check_device_rule_size(class_device1, source), 1);
    assert_int_equal(test_check_device_rule_size(class_device2, source), 1);

    amxc_var_delete(&device_params);
    classification_instance_delete(index1);
}
