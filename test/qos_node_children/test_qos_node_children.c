/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/
#include <stdlib.h>
#include <stdio.h>
#include <setjmp.h>
#include <stdarg.h>
#include <cmocka.h>

#include <amxc/amxc_macros.h>
#include <amxc/amxc.h>
#include <amxp/amxp.h>

#include <amxd/amxd_dm.h>
#include <amxd/amxd_object.h>
#include <amxd/amxd_transaction.h>
#include <amxd/amxd_object_event.h>

#include <amxo/amxo.h>

#include "qos-assert.h"
#include "dm_tr181-qos.h"

#include "libqosmodule_mock.h"
#include "test_qos_node_children.h"
#include "tr181-qos.h"
#include <qoscommon/qos-queue.h>
#include <qosnode/qos-node-api.h>
#include "qos.h"

static amxd_dm_t dm;
static amxo_parser_t parser;
static const char* odl_defs = "qos_node_children_test.odl";

static void handle_events(void) {
    while(amxp_signal_read() == 0) {
    }
}

int test_qos_node_children_setup(UNUSED void** state) {
    amxd_object_t* root_obj = NULL;

    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_PATH, "QoS.Node");
    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_INSTANCE, "QoS.Node.");
    assert_string_equal(QOS_TR181_DEVICE_QOS_NODE_OBJECT_NAME, "Node");

    assert_int_equal(amxd_dm_init(&dm), amxd_status_ok);
    assert_int_equal(amxo_parser_init(&parser), 0);

    root_obj = amxd_dm_get_root(&dm);
    assert_non_null(root_obj);

    assert_int_equal(amxo_resolver_ftab_add(&parser, "qos_main",
                                            AMXO_FUNC(_qos_main)), 0);

    assert_int_equal(amxo_parser_parse_file(&parser, odl_defs, root_obj), 0);

    assert_int_equal(_qos_main(0, &dm, &parser), 0);
    _qos_app_start(NULL, NULL, NULL);
    handle_events();

    return 0;
}

int test_qos_node_children_teardown(UNUSED void** state) {
    _qos_main(1, &dm, &parser);

    amxo_parser_clean(&parser);
    amxd_dm_clean(&dm);

    return 0;
}

void test_qos_node_children_simple_hierarchy(UNUSED void** state) {
    qos_node_t* shaper = qos_node_get_node("QoS.Node.node-shaper-default");
    qos_node_t* scheduler;

    assert_non_null(shaper);
    assert_string_equal(qos_node_dm_get_reference(shaper), "QoS.Shaper.shaper-default");

    scheduler = qos_node_find_child_by_type(shaper, QOS_NODE_TYPE_SCHEDULER);
    assert_non_null(scheduler);
    assert_string_equal(qos_node_dm_get_reference(scheduler), "QoS.Scheduler.scheduler-default");
    assert_ptr_equal(shaper, qos_node_find_parent_by_type(scheduler, QOS_NODE_TYPE_SHAPER));
}

void test_qos_node_children_multiple_hierarchy(UNUSED void** state) {
    int retval = -1;
    qos_node_t* queue_node = qos_node_get_node("QoS.Node.node-queue-1");
    amxc_llist_t* children;

    assert_non_null(queue_node);
    assert_string_equal(qos_node_dm_get_reference(queue_node), "QoS.Queue.queue-1");

    children = qos_node_find_children_by_type(queue_node, QOS_NODE_TYPE_QUEUE);
    assert_non_null(children);

    amxc_llist_it_t* it = amxc_llist_get_at(children, 0);
    qos_node_t* queue_node_child = qos_node_llist_it_get_node(it);

    assert_non_null(queue_node_child);
    assert_string_equal(qos_node_dm_get_alias(queue_node_child), "node-queue-2");
    assert_string_equal(qos_node_queue_get_alias(queue_node_child), "queue-2");
    assert_int_equal(qos_node_queue_get_precedence(queue_node_child), 1);
    assert_ptr_equal(queue_node, qos_node_find_parent_by_type(queue_node_child, QOS_NODE_TYPE_QUEUE));

    it = amxc_llist_get_at(children, 1);
    queue_node_child = qos_node_llist_it_get_node(it);
    assert_string_equal(qos_node_dm_get_alias(queue_node_child), "node-queue-3");
    assert_string_equal(qos_node_queue_get_alias(queue_node_child), "queue-3");
    assert_int_equal(qos_node_queue_get_precedence(queue_node_child), 2);
    assert_ptr_equal(queue_node, qos_node_find_parent_by_type(queue_node_child, QOS_NODE_TYPE_QUEUE));

    //Checking if the child gets activated if the parent activation fails.
    //In this case for the queue
    qos_queue_t* queue = queue_node->data.queue;
    qos_queue_t* queue_child2 = queue_node_child->data.queue;

    assert_non_null(queue);
    assert_int_equal(qos_queue_dm_get_status(queue_child2), QOS_STATUS_DISABLED);

    //__wrap_qos_module_exec_api_function fails.
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, -1);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, -1);
    assert_int_equal(qos_queue_dm_get_status(queue_child2), QOS_STATUS_DISABLED);

    //__wrap_qos_module_exec_api_function fails but gives a misconfigured error message. In this case 11
    expect_value(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE);
    expect_any(__wrap_qos_module_exec_api_function, module_name);
    will_return(__wrap_qos_module_exec_api_function, 11);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, -1);
    assert_int_equal(qos_queue_dm_get_status(queue_child2), QOS_STATUS_DISABLED);

    //__wrap_qos_module_exec_api_function succeeds
    expect_value_count(__wrap_qos_module_exec_api_function, id, QOS_MODULE_API_FUNC_ACTIVATE_QUEUE, 3);
    expect_any_always(__wrap_qos_module_exec_api_function, module_name);
    will_return_always(__wrap_qos_module_exec_api_function, 0);
    retval = qos_queue_activate(queue);
    assert_int_equal(retval, 0);
    assert_int_equal(qos_queue_dm_get_status(queue_child2), QOS_STATUS_ENABLED);

    qos_node_llist_delete(&children);
}
