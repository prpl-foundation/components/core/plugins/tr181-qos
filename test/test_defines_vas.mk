CFLAGS += -DDEVICE_BASED_QOS
LDFLAGS += -lamxb -lgmap-client
MOCK_WRAP_VAS = gmap_client_init \
			    gmap_query_open_ext \
			    gmap_query_close
LDFLAGS += $(addprefix $(WRAP_FUNC),$(MOCK_WRAP_VAS))